package com.kallipolis.model

data class AllFavoriteDeputiesResponseItem(
    val deputado: DeputyResponseItem,
    val id: Int,
    val idDeputado: Int,
    val idUsuario: Int
)