package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.FavoriteTopic
import com.kallipolis.model.LawPropositionFilter
import com.kallipolis.model.LawPropositionTopic
import com.kallipolis.repository.LawPropositionsRepository
import kotlinx.coroutines.*
import java.util.regex.Pattern
import java.util.regex.PatternSyntaxException

class PropositionsFilterViewModel: ViewModel() {

    private lateinit var originalListOfPropositionTopics: MutableList<LawPropositionTopic>

    private val _listOfPropositionTopics = MutableLiveData<MutableList<LawPropositionTopic>>()
    val listOfPropositionTopics: LiveData<MutableList<LawPropositionTopic>> = _listOfPropositionTopics

    private val _propositionsFilters = MutableLiveData<LawPropositionFilter?>()
    val propositionsFilters: LiveData<LawPropositionFilter?> = _propositionsFilters

    private val lawPropositionsRepository = LawPropositionsRepository()

    private val mapOfTopics = HashMap<String, LawPropositionTopic>()

    init {
        _propositionsFilters.value = LawPropositionFilter()
    }

    fun getAllPropositionTopics(userId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = lawPropositionsRepository.getAllLawPropositionThemes(userId)
            if (response.isSuccessful) {
                val propositionTopics = response.body()!!.lawPropositionTopics.toMutableList()
                originalListOfPropositionTopics = mutableListOf()
                originalListOfPropositionTopics.addAll(propositionTopics)
                buildHashMapOfTopics(propositionTopics)
                selectAllFavoriteTopics(propositionTopics)
                withContext(Dispatchers.Main) {
                    _listOfPropositionTopics.value = propositionTopics
                }
            }
        }
    }

     fun cleanAllFavoriteTopics(userId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = lawPropositionsRepository.getAllLawPropositionThemes(userId)
            if (response.isSuccessful) {
                val propositionTopics = response.body()!!.lawPropositionTopics.toMutableList()
                val favoriteTopicsIds = buildListOfFavoriteTopicsIds(propositionTopics)
                if (favoriteTopicsIds.size != 0) {
                    lawPropositionsRepository.deleteAllFavoritePropositionTopics(userId, favoriteTopicsIds)
                    getAllPropositionTopics(userId)
                }
            }
        }
    }

    fun addNewPropositionTopicToFilter(userId: Int, topic: LawPropositionTopic) {
        val newFavoriteTopic = FavoriteTopic(listOf<Int>(topic.id))
        CoroutineScope(Dispatchers.IO).launch {
            lawPropositionsRepository.addNewFavoritePropositionTopics(userId, newFavoriteTopic)
            updateFilterWithNewTopic(topic)
        }
    }

    fun removePropositionTopicFromFilter(userId: Int, topic: LawPropositionTopic) {
        CoroutineScope(Dispatchers.IO).launch {
            lawPropositionsRepository.deleteFavoritePropositionTopic(userId, topic.id)
            updateFilterRemovingTopic(topic)
        }
    }

    private fun buildListOfFavoriteTopicsIds(propositionTopics: MutableList<LawPropositionTopic>):
        MutableList<Int> {
        val favoriteTopicsIds = mutableListOf<Int>()
        for (topic in propositionTopics) {
            if (topic.favorite)
                favoriteTopicsIds.add(topic.id)
        }

        return favoriteTopicsIds
    }

    private fun buildHashMapOfTopics(propositionTopics: MutableList<LawPropositionTopic>) {
        CoroutineScope(Dispatchers.IO).launch {
            for (topic in propositionTopics) {
                mapOfTopics[topic.name] = topic
            }
        }
    }

    private fun selectAllFavoriteTopics(propositionTopics: MutableList<LawPropositionTopic>) {
        CoroutineScope(Dispatchers.IO).launch {
            for (topic in propositionTopics) {
                if (topic.favorite) {
                    updateFilterWithNewTopic(topic)
                }
            }
        }
    }

    fun getTopicByName(topic: String): LawPropositionTopic {
        return mapOfTopics[topic]!!
    }

    private suspend fun updateFilterWithNewTopic(topic: LawPropositionTopic) {
        if (_propositionsFilters.value == null) {
            withContext(Dispatchers.Main) {
                _propositionsFilters.value = LawPropositionFilter()
            }
        }

        withContext(Dispatchers.Main) {
            _propositionsFilters.value!!.addNewPropositionTopicToFilter(topic.id)
        }
    }

    private  fun updateFilterRemovingTopic(topic: LawPropositionTopic) {
        if (_propositionsFilters.value == null) {
            return
        }

        _propositionsFilters.value!!.removePropositionTopicFromFilter(topic.id)
    }

    fun hasValidFilter(): Boolean {
        return _propositionsFilters.value != null && _propositionsFilters.value!!.isValidFilter()
    }

    fun addDateRangeToFilter(startDate: Long, endDate: Long) {
        if (_propositionsFilters.value == null) {
            return
        }

        _propositionsFilters.value!!.addDateRangeToFilter(startDate, endDate)
    }

    fun matchTopicWithRegex(regex: String) {
        if (isInValidRegex(regex)) {
            restoreOriginalListOfPropositionTopics()
        } else {
            searchForTopicsMatchingWithRegex(regex)
        }
    }

    private fun searchForTopicsMatchingWithRegex(regex: String) {
        try {
            buildListWithTopicsMatchingWith(regex)
        } catch (e: PatternSyntaxException) {
            restoreOriginalListOfPropositionTopics()
            println(e)
        } finally {
            return
        }
    }

    private fun isInValidRegex(regex: String) = regex.isNullOrEmpty() || regex.isBlank()

    private fun restoreOriginalListOfPropositionTopics() {
        _listOfPropositionTopics.value = originalListOfPropositionTopics
    }

    private fun buildListWithTopicsMatchingWith(regex: String)  {
        val newListWithMatchedTopics = mutableListOf<LawPropositionTopic>()
        val pattern = Pattern.compile(regex)
        for (topic in originalListOfPropositionTopics) {
            val matcher = pattern.matcher(topic.name)
            if (matcher.find()) {
                newListWithMatchedTopics.add(topic)
            }
        }
        _listOfPropositionTopics.value = newListWithMatchedTopics
    }
}