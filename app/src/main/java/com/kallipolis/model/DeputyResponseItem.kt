package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class DeputyResponseItem(
    @SerializedName("email")
    val email: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("nome")
    val name: String,
    @SerializedName("siglaPartido")
    val partyInitials: String,
    @SerializedName("siglaUf")
    val stateInitials: String,
    @SerializedName("uri")
    val uri: String,
    @SerializedName("uriPartido")
    val uriParty: String,
    @SerializedName("urlFoto")
    val urlProfilePicture: String,
    @SerializedName("favorito")
    var isFavorite: Boolean
)