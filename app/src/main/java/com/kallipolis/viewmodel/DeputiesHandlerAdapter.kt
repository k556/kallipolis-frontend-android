package com.kallipolis.viewmodel

interface DeputiesHandlerAdapter {
    fun deleteFavoriteDeputy(userId: Int, currentDeputyId: Int)
    fun addNewFavoriteDeputy(userId: Int, currentDeputyId: Int)
    fun showDeputyDetails(deputyId: Int)
}