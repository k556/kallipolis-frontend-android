package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class DeputyExpenseDetailsResponse(
    @SerializedName("ano")
    val year: Int,
    @SerializedName("cnpjCpfFornecedor")
    val providerDocuments: String,
    @SerializedName("codDocumento")
    val documentCode: Int,
    @SerializedName("codLote")
    val batchCode: Int,
    @SerializedName("codTipoDocumento")
    val documentTypeCode: Int,
    @SerializedName("dataDocumento")
    val documentDate: String,
    @SerializedName("mes")
    val month: Int,
    @SerializedName("nomeFornecedor")
    val providerName: String,
    @SerializedName("numDocumento")
    val documentNumber: String,
    @SerializedName("numRessarcimento")
    val refundNumber: String,
    @SerializedName("parcela")
    val installments: Int,
    @SerializedName("tipoDespesa")
    val expenseType: String,
    @SerializedName("tipoDocumento")
    val documentType: String,
    @SerializedName("urlDocumento")
    val documentUrl: String,
    @SerializedName("valorDocumento")
    val documentValue: Double,
    @SerializedName("valorGlosa")
    val glossValue: Double,
    @SerializedName("valorLiquido")
    val netValue: Double
)