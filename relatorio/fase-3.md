# Monitor Político - Kallipolis
<!-- ![alt_text](images/image1.png "image_tooltip") -->

- [Monitor Político - Kallipolis](#monitor-político---kallipolis)
  - [Introdução](#introdução)
    - [Participantes do Grupo](#participantes-do-grupo)
    - [Requisitos definidos para a entrega](#requisitos-definidos-para-a-entrega)
  - [Descrição](#descrição)
  - [Funcionalidades](#funcionalidades)
    - [Detalhes Proposições](#Detalhes-Proposições)
    - [Votações em proposições](#Votações-em-proposições)


## Introdução
Este o relatório da disciplina MAC5714 - Tópicos Avançados de Programação Orientada a Objetos do
programa de Pós Graduação em Ciência da Computação do IME/USP ministrada pelo Professor Dr. Fábio
Kon.

O objetivo desse projeto é auxiliar usuários a localizar e organizar informações sobre a atividade
parlamentar dos deputados e partidos. Para isto desenvolvemos e disponibilizamos um
[aplicativo Android na PlayStore](https://play.google.com/store/apps/details?id=com.kallipolis) que
possibilita a usuários escolher deputados e partidos ele deseja monitorar.

Caso procure detalhes da implementação desenvolvida, é indicado olhar nos repositórios dedicados
do [frontend](https://gitlab.com/k556/kallipolis-frontend-android) e do
[backend](https://gitlab.com/k556/kallipolis-backend) da aplicação.

### Participantes do Grupo

| Nome                   | NUSP    |
|------------------------|---------|
| Alex Barboza           |         |
| Bruno Assis            |         |
| Hussani Oliveira       |         |
| Kouao Jean Vincent Mea | 9253169 |


## Descrição

Data de entrega: 26/11/2021

### Requisitos definidos para a entrega
3. O sistema deve montar e mostrar de forma bonita e bem organizada os votos de acordo com as temáticas indicadas no item 3).

Nas entregas anteriores, foi possível listar as proposições de acordo com os temas desejados pelo usuário. 
Nesta entrega iremos apresentar as funcionalidades que permitirão ao usuario de visualizar detalhes da proposição e como cada deputado votou ou como cada partido orientou seus deputados a votar naquela proposição.

Conforme especificado acima, foram definidas as seguintes funcionalidades que um usuário poderá executar no sistema.

- [Detalhes Proposições](#Detalhes-Proposições)
- [Votações em proposições](#Votações-em-proposições)

## Funcionalidades

### Detalhes Proposições

Esta funcionalidade permite ao usuário de visualizar uma breve introdução sobre aquela proposição selecionada.

![Tela de detalhes da proposição](images/fase-3/detalhes.jpg "image_tooltip")

Figura 1: Tela de detalhes da proposição.

### Votações em proposições

De acordo com a proposição selecionada, o usuário consegue saber como cada deputado votou (Figura 2) ou qual foi a orientação do partido (Figura 3).

Clicando na aba votação, o botão de deputado é selecionado por default e é possível visualizar esta tela.

![Tela exibindo os votos por deputado](images/fase-3/votos_deputados.png "image_tooltip")

Figura 2: Tela exibindo como cada deputado votou na proposta.

Clicando no botão de partidos, exibe-se esta tela.

![Tela exibindo os votos segundo a orientação por partido](images/fase-3/votos_partidos.png "image_tooltip")

Figura 3: Tela exibindo informações sobre a orientação de voto dada pelo partido.

Para cada opção (deputado ou partido) é possível saber o número de votações que foram a favor e contra a proposta.



