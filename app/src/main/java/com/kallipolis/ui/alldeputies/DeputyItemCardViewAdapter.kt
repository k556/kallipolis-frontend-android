package com.kallipolis.ui.alldeputies

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kallipolis.DeputyDetailsActivity
import com.kallipolis.R
import com.kallipolis.databinding.DeputyListItemBinding
import com.kallipolis.model.DeputyResponseItem
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.DeputiesHandlerAdapter

class DeputyItemCardViewAdapter
    (private var allDeputies: ArrayList<DeputyResponseItem>,
     private val deputiesHandlerAdapter: DeputiesHandlerAdapter,
     private val userId: Int):
    RecyclerView.Adapter<DeputyItemCardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeputyItemCardViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: DeputyListItemBinding = DataBindingUtil
            .inflate(layoutInflater, R.layout.deputy_list_item, parent, false)
        return DeputyItemCardViewHolder(binding, parent.context, deputiesHandlerAdapter, userId)
    }

    override fun onBindViewHolder(holder: DeputyItemCardViewHolder, position: Int) {
        holder.bind(allDeputies[position])
    }

    override fun getItemCount(): Int {
        return allDeputies.size
    }

    fun updateData(newItems: ArrayList<DeputyResponseItem>) {
        this.allDeputies = newItems
        this.notifyDataSetChanged()
    }

    fun setNewFavorite(position: Int) {
        val newFavoriteDeputy = this.allDeputies[position]
        newFavoriteDeputy.isFavorite = true
        this.notifyDataSetChanged()
    }

    fun unsetFavorite(position: Int) {
        val deputy = this.allDeputies[position]
        deputy.isFavorite = false
        this.notifyDataSetChanged()
    }
}

class DeputyItemCardViewHolder(val binding: DeputyListItemBinding,
                               val context: Context,
                               val deputiesHandlerAdapter: DeputiesHandlerAdapter,
                               val userId: Int):
    RecyclerView.ViewHolder(binding.root) {
    fun bind(deputy: DeputyResponseItem) {
        binding.deputyItemName.text = deputy.name
        binding.deputyItemParty.text = "${deputy.partyInitials} · ${deputy.stateInitials}"
        Glide.with(context)
            .load(deputy.urlProfilePicture)
            .into(binding.deputyItemProfilePicture)

        if (deputy.isFavorite) {
            binding.icFavoriteDeputy.setImageResource(R.drawable.ic_icon_filled)
        } else {
            binding.icFavoriteDeputy.setImageResource(R.drawable.ic_icon_empty)
        }

        binding.icFavoriteDeputy.setOnClickListener {
            val currentDeputyId = deputy.id
            if (deputy.isFavorite) {
                deputiesHandlerAdapter.deleteFavoriteDeputy(userId, currentDeputyId)
            } else {
                deputiesHandlerAdapter.addNewFavoriteDeputy(userId, currentDeputyId)
            }
        }

        binding.deputyDetailsContainer.setOnClickListener {
            val intent = Intent(context, DeputyDetailsActivity::class.java)
            intent.putExtra(StorageUtils.DEPUTY_ID_PARAMETER, deputy.id)
            intent.putExtra(StorageUtils.DEPUTY_NAME_PARAMETER, deputy.name)
            context.startActivity(intent)
        }

    }
}