package com.kallipolis.https

import com.kallipolis.model.PoliticalPartyDetails
import com.kallipolis.model.PoliticalPartyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface PoliticalPartyService {
    @GET("/usuario/{userId}/partido_favorito/all")
    suspend fun fetchAllPoliticalParties(@Path("userId") userId: Int): Response<PoliticalPartyResponse>

    @GET("/partido/{partyId}")
    suspend fun fetchPoliticalPartyDetails(@Path("partyId") partyId: Int): Response<PoliticalPartyDetails>
}