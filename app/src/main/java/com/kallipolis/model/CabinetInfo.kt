package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class CabinetInfo(
    @SerializedName("andar")
    val floor: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("nome")
    val cabinetName: String,
    @SerializedName("predio")
    val building: String,
    @SerializedName("sala")
    val room: String,
    @SerializedName("telefone")
    val phoneNumber: String
)