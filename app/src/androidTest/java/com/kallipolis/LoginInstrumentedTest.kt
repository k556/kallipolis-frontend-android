package com.kallipolis

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.hamcrest.Matchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.Spy

@RunWith(AndroidJUnit4::class)
class LoginInstrumentedTest {
    @get:Rule
    var activityRule: ActivityTestRule<LoginActivity> = ActivityTestRule(LoginActivity::class.java)

    @Test
    fun allViewsOnLoginScreenAreDisplayed() {
        onView(withId(R.id.login_et_email))
            .check(matches(isDisplayed()))

        onView(withId(R.id.login_btn_send_email))
            .check(matches(isDisplayed()))

        onView(withId(R.id.login_btn_send_email))
            .check(matches(isDisplayed()))
    }

    @Test
    fun insertInvalidEmailShouldDisableLoginButton() {
        onView(withId(R.id.login_et_name))
            .perform(clearText(), typeText("Alex"))
        onView(withId(R.id.login_et_name))
            .perform(closeSoftKeyboard())
        Thread.sleep(1000)

        onView(withId(R.id.login_et_email))
            .perform(clearText(), typeText("alex.barboza@"))
        onView(withId(R.id.login_et_email))
            .perform(closeSoftKeyboard())
        Thread.sleep(1000)

        onView(withId(R.id.login_btn_send_email))
            .check(matches(not(isEnabled())))
    }

    @Test
    fun insertValidEmailShouldEnableLoginButton() {
        onView(withId(R.id.login_et_name))
            .perform(clearText(), typeText("Alex"))
        onView(withId(R.id.login_et_name))
            .perform(closeSoftKeyboard())
        Thread.sleep(1000)

        onView(withId(R.id.login_et_email))
            .perform(clearText(), typeText("alex.barboza@usp.br"))
        onView(withId(R.id.login_et_email))
            .perform(closeSoftKeyboard())
        Thread.sleep(1000)

        onView(withId(R.id.login_btn_send_email))
            .check(matches(isEnabled()))
    }

}