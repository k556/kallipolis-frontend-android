package com.kallipolis

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.WindowManager
import androidx.core.content.res.ResourcesCompat
import androidx.core.util.Pair
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import co.lujun.androidtagview.ColorFactory
import co.lujun.androidtagview.TagContainerLayout
import co.lujun.androidtagview.TagView
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.TextInputEditText
import com.kallipolis.databinding.ActivityPropositionsFilterBinding
import com.kallipolis.model.LawPropositionFilter
import com.kallipolis.model.LawPropositionTopic
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.PropositionsFilterViewModel

class PropositionsFilterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPropositionsFilterBinding
    private lateinit var propositionsFilterViewModel: PropositionsFilterViewModel
    private lateinit var popupPropositionsDatePicker: MaterialDatePicker<Pair<Long, Long>>
    private var userId: Int = 0

    companion object {
        const val TAG_TEXT_SIZE = 30f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_propositions_filter)
        propositionsFilterViewModel = ViewModelProvider(this).get(PropositionsFilterViewModel::class.java)
        initCurrentUserData()
        initViews()
        initObservers()
        initViewData()
    }

    private fun initCurrentUserData() {
        userId = retrieveUserId()
    }

    private fun initViews() {
        setStatusBar()
        initTagsStyle()
        initDateRangePicker()
        initButtonsOnClickListeners()
        binding.propositionsTopicsSearch.setStartIconDrawable(R.drawable.ic_search_field)
        val onRegexChangedListener = FilterPropositionEditTextListener(propositionsFilterViewModel,
            binding.propositionsTopicsEtFilter)
        binding.propositionsTopicsEtFilter.addTextChangedListener(onRegexChangedListener)
    }

    private fun initObservers() {
        propositionsFilterViewModel.listOfPropositionTopics.observe(this, {
            setTagStyleForEachTopic(it)
            setOnTagClickListeners()
        })
    }

    private fun initViewData() {
        propositionsFilterViewModel.getAllPropositionTopics(userId)
    }

    private fun setStatusBar() {
        actionBar?.hide()
        supportActionBar?.hide()
        val window = this.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = this.resources.getColor(R.color.status_bar_color)
    }

    private fun initTagsStyle() {
        binding.propositionsTagsList.theme = ColorFactory.NONE
        binding.propositionsTagsList.backgroundColor = Color.WHITE
        binding.propositionsTagsList.tagBackgroundColor = Color.WHITE
        binding.propositionsTagsList.tagTextColor = Color.GRAY
        binding.propositionsTagsList.tagBorderColor = Color.GRAY
        val typeface = ResourcesCompat.getFont(this, R.font.ubuntu_light)
        binding.propositionsTagsList.tagTypeface = typeface
        binding.propositionsTagsList.tagTextSize = TAG_TEXT_SIZE
        binding.propositionsTagsList.tag
    }

    private fun initDateRangePicker() {
        val pickerBuilder = MaterialDatePicker.Builder.dateRangePicker()
        pickerBuilder.setTheme(R.style.PropositionsCalendarTimeRangeTheme)
        popupPropositionsDatePicker = pickerBuilder.build()

        popupPropositionsDatePicker.addOnDismissListener {
            popupPropositionsDatePicker.dismiss()
        }

        popupPropositionsDatePicker.addOnCancelListener {
            popupPropositionsDatePicker.dismiss()
        }

        popupPropositionsDatePicker.addOnPositiveButtonClickListener {
            propositionsFilterViewModel.addDateRangeToFilter(it.first, it.second)
            val currentFilter = propositionsFilterViewModel.propositionsFilters.value!!
            val startDate = currentFilter.brazilianFormatStartDate
            val endDate = currentFilter.brazilianFormatEndDate
            val selectedDateRange = "$startDate - $endDate"
            binding.popupPropositionsDateRangeView.text = selectedDateRange
        }

        binding.datePickerContainer.setOnClickListener {
            popupPropositionsDatePicker.show(supportFragmentManager, popupPropositionsDatePicker.toString())
        }
    }

    private fun initButtonsOnClickListeners() {
        binding.imgPropositionsFilterClose.setOnClickListener {
            super.onBackPressed()
        }

        binding.popupPropositionsFilterClean.setOnClickListener {
            cleanAllFiltersAndGoBackToPropositions()
        }

        binding.popupPropositionsFilterApply.setOnClickListener {
            if (propositionsFilterViewModel.hasValidFilter()) {
                val filter = propositionsFilterViewModel.propositionsFilters.value!!
                applyFilterAndLoadFilteredPropositions(filter)
            } else {
                cleanAllFiltersAndGoBackToPropositions()
            }
        }
    }

    private fun cleanAllFiltersAndGoBackToPropositions() {
        propositionsFilterViewModel.cleanAllFavoriteTopics(userId)
        goBackToPropositions()
    }

    private fun applyFilterAndLoadFilteredPropositions(filter: LawPropositionFilter) {
        val bundle = Bundle()
        val intent = Intent(this, HomeActivity::class.java)

        bundle.putParcelable(StorageUtils.PROPOSITIONS_FILTER_APPLY, filter)
        intent.putExtra(StorageUtils.PROPOSITIONS_FILTER_APPLY, bundle)
        intent.putExtra(StorageUtils.SHOULD_NAVIGATE_PROPOSITIONS, true)

        startActivity(intent)
    }

    private fun goBackToPropositions() {
        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra(StorageUtils.SHOULD_NAVIGATE_PROPOSITIONS, true)
        startActivity(intent)
    }

    private fun setTagStyleForEachTopic(propositions: MutableList<LawPropositionTopic>) {
        binding.propositionsTagsList.removeAllTags()

        for ((i, topic) in propositions.withIndex()) {
            binding.propositionsTagsList.addTag(topic.name)
            val currentTag = binding.propositionsTagsList.getTagView(i)
            if (topic.favorite) {
                setSelectedTagStyle(currentTag)
            } else {
                setUnselectedTagStyle(currentTag)
            }
        }
    }

    private fun setOnTagClickListeners() {
        val clickHandler = PropositionTopicTagClickListener(
            binding.propositionsTagsList,
            propositionsFilterViewModel)
        binding.propositionsTagsList.setOnTagClickListener(clickHandler)
    }

    private fun retrieveUserId():Int {
        return StorageUtils.retrieveUserId(this)
    }

    private fun setSelectedTagStyle(tag: TagView) {
        tag.tagBackgroundColor = Color.GRAY
        tag.setTagTextColor(Color.WHITE)
    }

    private fun setUnselectedTagStyle(tag: TagView) {
        tag.tagBackgroundColor = Color.WHITE
        tag.setTagTextColor(Color.GRAY)
    }

    private inner class PropositionTopicTagClickListener(val tagContainerLayout: TagContainerLayout,
                                                         val propositionsFilterViewModel: PropositionsFilterViewModel
    )
        : TagView.OnTagClickListener {
        override fun onTagClick(position: Int, text: String?) {
            val clickedTag = tagContainerLayout.getTagView(position)
            val clickedTopic = propositionsFilterViewModel.getTopicByName(text!!)
            if (clickedTopic.favorite) {
                setUnselectedTagStyle(clickedTag)
                propositionsFilterViewModel.removePropositionTopicFromFilter(userId, clickedTopic)
            } else {
                setSelectedTagStyle(clickedTag)
                propositionsFilterViewModel.addNewPropositionTopicToFilter(userId, clickedTopic)
            }
        }

        override fun onTagLongClick(position: Int, text: String?) {
            /*DO NOTHING*/
        }

        override fun onSelectedTagDrag(position: Int, text: String?) {
            /*DO NOTHING*/
        }

        override fun onTagCrossClick(position: Int) {
            /*DO NOTHING*/
        }
    }

    private inner class FilterPropositionEditTextListener(
        val propositionsFilterViewModel: PropositionsFilterViewModel,
        val editText: TextInputEditText): TextWatcher {

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            /*DO NOTHING*/
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            /*DO NOTHING*/
        }

        override fun afterTextChanged(p0: Editable?) {
            propositionsFilterViewModel.matchTopicWithRegex(p0.toString())
        }

    }

}