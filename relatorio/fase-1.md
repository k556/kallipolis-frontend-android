# Monitor Político - Kallipolis
<!-- ![alt_text](images/image1.png "image_tooltip") -->

- [Monitor Político - Kallipolis](#monitor-político---kallipolis)
  - [Introdução](#introdução)
    - [Participantes do Grupo](#participantes-do-grupo)
    - [Requisitos definidos para a entrega](#requisitos-definidos-para-a-entrega)
  - [Descrição](#descrição)
  - [Funcionalidades](#funcionalidades)
    - [Cadastro](#cadastro)
    - [Favoritar deputados](#favoritar-deputados)
    - [Favoritar partidos](#favoritar-partidos)
    - [Visualizar deputados favoritados](#visualizar-deputados-favoritados)
    - [Visualizar partidos favoritados](#visualizar-partidos-favoritados)

## Introdução
Este o relatório da disciplina MAC5714 - Tópicos Avançados de Programação Orientada a Objetos do
programa de Pós Graduação em Ciência da Computação do IME/USP ministrada pelo Professor Dr. Fábio
Kon.

O objetivo desse projeto é auxiliar usuários a localizar e organizar informações sobre a atividade
parlamentar dos deputados e partidos. Para isto desenvolvemos e disponibilizamos um
[aplicativo Android na PlayStore](https://play.google.com/store/apps/details?id=com.kallipolis) que
possibilita a usuários escolher deputados e partidos ele deseja monitorar.

Caso procure detalhes da implementação desenvolvida, é indicado olhar nos repositórios dedicados
do [frontend](https://gitlab.com/k556/kallipolis-frontend-android) e do
[backend](https://gitlab.com/k556/kallipolis-backend) da aplicação.

### Participantes do Grupo

| Nome                   | NUSP    |
|------------------------|---------|
| Alex Barboza           |         |
| Bruno Assis            |         |
| Caroline Araújo        |         |
| Gustavo Oliveira       |         |
| Hussani Oliveira       |         |
| Kouao Jean Vincent Mea | 9253169 |


## Descrição

Data de entrega: 15/10/2021

### Requisitos definidos para a entrega
1. Por meio de uma página Web do seu sistema, um usuário deve ser capaz de fornecer (A) uma lista
   de partidos políticos que ele gostaria de monitorar e (B) uma lista de deputados federais que
   ele gostaria de monitorar.
2. O sistema deve permitir remover e listar partidos e deputados, sempre de forma intuitiva e
   visualmente agradável.

Conforme especificado nos requisitos acima, foram definidas as seguintes funcionalidades que um usuário poderá executar no sistema.

- [Cadastro](#cadastro)
- [Favoritar deputados](#favoritar-deputados)
- [Favoritar partidos](#favoritar-partidos)
- [Visualizar deputados favoritados](#visualizar-deputados-favoritados)
- [Visualizar partidos favoritados](#visualizar-partidos-favoritados)

## Funcionalidades

### Cadastro

Conforme a figura a seguir, o usuário poderá se cadastrar, informando o seu nome completo e o seu
email.


![Tela de Cadastro do aplicativo](images/fase-1/image2.jpg "image_tooltip")

Figura 1: Tela de Cadastro do aplicativo.

### Favoritar deputados

O usuário pode favoritar deputados de acordo com sua preferência.

![Tela exibindo como favoritar um deputado](images/fase-1/image3.jpg "image_tooltip")

Figura 2: Tela exibindo como favoritar um deputado.

### Favoritar partidos

O usuário pode favoritar partidos de acordo com sua preferência.

![Tela exibindo como favoritar um partido](images/fase-1/image4.jpg "image_tooltip")

Figura 3: Tela exibindo como favoritar um partido.

### Visualizar deputados favoritados

Esta tela exibe a lista dos deputados favoritos.

![Tela mostrando os deputados favoritos](images/fase-1/image5.jpg "image_tooltip")

Figura 4: Tela mostrando os deputados favoritos.

### Visualizar partidos favoritados

Esta tela exibe a lista dos partidos favoritos.

![Tela mostrando os partidos favoritos](images/fase-1/image6.jpg "image_tooltip")

Figura 5: Tela mostrando os partidos favoritos.
