package com.kallipolis.https

import com.kallipolis.model.FavoriteTopic
import com.kallipolis.model.LawPropositionDetailsResponse
import com.kallipolis.model.LawPropositionsResponse
import com.kallipolis.model.LawPropositionsThemes
import retrofit2.Response
import retrofit2.http.*

interface LawPropositionsService {
    @GET("proposicao")
    suspend fun fetchAllLawPropositions(@Query("page") page: Int,
                                        @Query("size") size: Int):
            Response<LawPropositionsResponse>

    @GET
    suspend fun fetchLawPropositionsWithFilters(@Url url: String):
            Response<LawPropositionsResponse>

    @GET("proposicao/{propositionId}")
    suspend fun fetchLawPropositionDetails(@Path("propositionId") propositionId: Int):
            Response<LawPropositionDetailsResponse>

    @GET("usuario/{userId}/votacao/topicos")
    suspend fun fetchAllLawPropositionTopics(@Path("userId") userId: Int):
            Response<LawPropositionsThemes>

    @POST("usuario/{userId}/votacao/topicos/favorito")
    suspend fun addNewFavoritePropositionTopics(@Path("userId") userId: Int,
                                                @Body favoriteTopic: FavoriteTopic):
            Response<LawPropositionsThemes>

    @DELETE("usuario/{userId}/votacao/topicos/favorito")
    suspend fun deleteFavoritePropositionTopic(@Path("userId") userId: Int,
                                               @Query("topicos") topicId: Int):
            Response<Unit>

    @DELETE
    suspend fun deleteAllFavoritePropositionTopics(@Url url: String): Response<Unit>
}