# Monitor Político - Kallipolis
<!-- ![alt_text](images/image1.png "image_tooltip") -->

- [Monitor Político - Kallipolis](#monitor-político---kallipolis)
  - [Introdução](#introdução)
    - [Participantes do Grupo](#participantes-do-grupo)
  - [Descrição](#descrição)
    - [Requisitos definidos para a entrega](#requisitos-definidos-para-a-entrega)
  - [Funcionalidades](#funcionalidades)
    - [Expressões regulares para tópicos de votação](#expressões-regulares-para-tópicos-de-votação)
    - [Nuvem de palavras](#nuvem-de-palavras)
    - [Bonus - Relatório de gastos](#bonus---relatório-de-gastos)
  - [Atualizações](#atualizações)
    - [Troca do componente de calendário](#troca-do-componente-de-calendário)
    - [Persistência dos tópicos de votação](#persistência-dos-tópicos-de-votação)


## Introdução
Este o relatório da disciplina MAC5714 - Tópicos Avançados de Programação Orientada a Objetos do
programa de Pós Graduação em Ciência da Computação do IME/USP ministrada pelo Professor Dr. Fábio
Kon.

O objetivo desse projeto é auxiliar usuários a localizar e organizar informações sobre a atividade
parlamentar dos deputados e partidos. Para isto desenvolvemos e disponibilizamos um
[aplicativo Android na PlayStore](https://play.google.com/store/apps/details?id=com.kallipolis) que
possibilita a usuários escolher deputados e partidos ele deseja monitorar.

Caso procure detalhes da implementação desenvolvida, é indicado olhar nos repositórios dedicados
do [frontend](https://gitlab.com/k556/kallipolis-frontend-android) e do
[backend](https://gitlab.com/k556/kallipolis-backend) da aplicação.

### Participantes do Grupo

| Nome                   | NUSP    |
|------------------------|---------|
| Alex Barboza           |         |
| Bruno Assis            |         |
| Hussani Oliveira       |         |
| Kouao Jean Vincent Mea | 9253169 |


## Descrição

Data de entrega: 17/12/2021

### Requisitos definidos para a entrega

1. Ao definir as palavras-chave e expressões, o usuário deve poder usar expressões regulares
2. De uma forma bonita e elegante, o sistema deve mostrar duas nuvens de palavras distintas
representando os votos SIM e NÃO das entidades selecionadas (partidos ou deputados). O sistema deve também mostrar uma nuvem de palavras representando os projeto de lei propostos por cada entidade selecionada.
3. Opcional (ponto bônus): Sugira uma nova funcionalidade ainda não especificada acima. Ou então, incorpore no sistema um relato dos gastos de um partido ou deputados de acordo com
https://www2.camara.leg.br/transparencia/receitas-e-despesas/despesas/pesquisa-de-despesas.



Conforme especificado acima, foram definidas as seguintes funcionalidades que um usuário poderá executar no sistema.

- [Detalhes Proposições](#Detalhes-Proposições)
- [Votações em proposições](#Votações-em-proposições)

## Funcionalidades

### Expressões regulares para tópicos de votação

Esta funcionalidade permite o usuário filtrar quais tópicos de votação serão exibidos utilizando expressões regulares.

![Tela de busca utilizando expressão regular para filtrar tópicos de votação](images/fase-4/filtro-topicos-com-expressao-regular.jpeg "image_tooltip")

Figura 1: Tela de busca utilizando expressão regular para filtrar tópicos de votação

Com a flexibilidade das expressões regulares é possível fazer as mais diversas opções de filtros. Abaixo estão listados alguns exemplos.
1. `[ç]`  exibe todos os tópicos que tenham a letra c cedilha  no nome
2. `^[ME]` exibe todos os tópicos que começam com a letra M ou letra E
3. `[o]$` exibe todos os tópicos que terminam com a letra o
4. `[^o]$` exibe todos os tópicos que não terminam com a letra o
5. `Direito` exibe todos os tópicos que contenham a palavra "Direito"

É importante salientar que esta funcionalidade é sensível a espaços e distingue entre maiúsculas, minúsculas e acentuação.

Os tópicos de votação, quando tem um tamanho grande, podem ser truncados nos botões. As expressões regulares do filtro desconsideram este trucamento da exibição. Desta forma, ao pesquisar pelo termo `Religião`, ou `[o]$` o botão "Arte, Cultura e Reli..." será exibido, dado que o termo filtrado foi "Familia, Direito e Religião".

### Nuvem de palavras

O usuário pode visualizar quais tópicos de votação determinado deputado como mais votou. Os tópicos mais votados são exibidos com fonte maior. 

![Tela exibindo nuvem de palavras que representa tópicos mais votados por deputado](images/fase-4/nuvem-palavras.jpg "image_tooltip")

Figura 2: Tela exibindo nuvem de palavras que representa tópicos mais votados por deputado

### Bonus - Relatório de gastos

O usuário pode visualizar detalhes sobre os gastos de determinado deputado. No aplicativo, conforme exibido na Figura 3, é possível ver o gasto total de um deputado e também os detalhes sobre natureza e valor de cada gasto individual.

![Tela exibindo relatório de gastos por deputado](images/fase-4/relatorio-gastos-deputado.jpeg "image_tooltip")

Figura 3: Tela exibindo relatório de gastos por deputado

## Atualizações

Nesta seção serão listadas atualizações que melhoram a experiência do usuário, mas que não representam funcionalidades adicionais para os usuários.

### Troca do componente de calendário

Visando maior aderência ao design system utilizado no restante do aplicativo, o componente de calendário foi trocado.

![Calendário utilizado nas versões anteriores](images/fase-4/calendario-antes.jpeg "image_tooltip")

Figura 4: Calendário utilizado nas versões anteriores

![Calendário utilizado nas versões anteriores](images/fase-4/calendario-depois.jpeg "image_tooltip")

Figura 5: Calendário utilizado na versão atual

### Persistência dos tópicos de votação

Nas versões anteriores do aplicativo os filtros de tópicos de votação não eram salvos. Como resultado disto, o usuário precisava selecionar os filtros sempre que acessava a página de proposições.

Nesta versão, os filtros ficam salvos na perfil do usuário. Desta forma os filtros serão mantidos entre diferentes sessões do aplicativo. Este comportamento é demonstrado no vídeo abaixo

<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="images/fase-4/filtros-persistentes.png">
    <source src="images/fase-4/filtros-persistentes.mp4" type="video/mp4">
  </video>
</figure>
<!-- blank line -->

Video 1: Filtros persistentes

O botão "Limpar" remove todos os filtros selecionados previamente pelo usuário.

O botão _"X"_ exibido no canto superior direito ignora as modificações feitas e retorna o usuário para a lista de resultados.
