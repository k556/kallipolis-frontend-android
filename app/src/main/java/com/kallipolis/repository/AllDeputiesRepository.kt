package com.kallipolis.repository

import com.kallipolis.https.DetailsService
import com.kallipolis.https.FederalDeputiesService
import com.kallipolis.https.ParliamentRetrofitInstance
import com.kallipolis.https.RetrofitInstance

class AllDeputiesRepository {

    companion object {
        const val PAGE_SIZE: Int = 15
    }

    private var allDeputiesWebService: FederalDeputiesService = RetrofitInstance
        .getRetrofitSingletonInstance()
        .create(FederalDeputiesService::class.java)


    private var parliamentWebservice: DetailsService = ParliamentRetrofitInstance
        .getParliamentRetrofitSingletonInstance()
        .create(DetailsService::class.java)

    suspend fun getAllDeputies(userId: Int, currentPage: Int) =
        allDeputiesWebService.fetchAllFederalDeputies(userId, currentPage, PAGE_SIZE)

    suspend fun getDeputyDetails(deputyId: Int) = parliamentWebservice.fetchDeputyDetails(deputyId)

    suspend fun getDeputyExpenses(deputyId: Int) = allDeputiesWebService.fetchDeputyExpenses(deputyId)
}