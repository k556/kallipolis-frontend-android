package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.AllFavoriteDeputiesResponse
import com.kallipolis.model.DeputyResponseItem
import com.kallipolis.repository.FavoritesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavoriteDeputiesViewModel: ViewModel(), DeputiesHandlerAdapter {
    private val _allFavoriteDeputies = MutableLiveData<MutableList<DeputyResponseItem>>()
    val allFavoriteDeputies: LiveData<MutableList<DeputyResponseItem>> = _allFavoriteDeputies

    private val favoritesRepository = FavoritesRepository()

    init {
        _allFavoriteDeputies.value = mutableListOf()
    }

    fun getAllFavoriteDeputies(userId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val allFavoriteDeputies = favoritesRepository.getAllFavoriteDeputies(userId)
            if (!allFavoriteDeputies!!.isEmpty()) {
                withContext(Dispatchers.Main) {
                    _allFavoriteDeputies.value = buildListOfFavoriteDeputies(allFavoriteDeputies)
                }
            } else {
                withContext(Dispatchers.Main) {
                    _allFavoriteDeputies.value = mutableListOf()
                }
            }
        }
    }

    private fun buildListOfFavoriteDeputies(response: AllFavoriteDeputiesResponse?): MutableList<DeputyResponseItem> {
        val listOfFavoriteDeputies = mutableListOf<DeputyResponseItem>()
        for (favoriteDeputy in response!!) {
            val currentFavoriteDeputy: DeputyResponseItem = favoriteDeputy.deputado
            currentFavoriteDeputy.isFavorite = true
            listOfFavoriteDeputies.add(currentFavoriteDeputy)
        }
        return listOfFavoriteDeputies
    }

    override fun deleteFavoriteDeputy(userId: Int, currentDeputyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            favoritesRepository.deleteFavoriteDeputy(userId, currentDeputyId)
            getAllFavoriteDeputies(userId)
        }
    }

    override fun addNewFavoriteDeputy(userId: Int, currentDeputyId: Int) {
       CoroutineScope(Dispatchers.IO).launch {
           favoritesRepository.addNewFavoriteDeputy(userId, currentDeputyId)
           getAllFavoriteDeputies(userId)
       }
    }

    override fun showDeputyDetails(deputyId: Int) {
        /*DO NOTHING*/
    }

}