package com.kallipolis.https

import com.kallipolis.model.User
import com.kallipolis.model.UserResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface UserLoginService {
    @POST("/usuario")
    suspend fun createUser(@Body user: User): Response<UserResponse>
}