package com.kallipolis.repository

import com.kallipolis.https.RetrofitInstance
import com.kallipolis.https.UserLoginService
import com.kallipolis.model.User

class UserRepository {
    private var userWebservice: UserLoginService = RetrofitInstance
        .getRetrofitSingletonInstance()
        .create(UserLoginService::class.java)

    suspend fun createUser(user: User) = userWebservice.createUser(user)

}