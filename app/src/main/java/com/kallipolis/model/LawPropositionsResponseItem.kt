package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class LawPropositionsResponseItem(
    @SerializedName("ano")
    val year: Int,
    @SerializedName("codTipo")
    val idPropositionType: String,
    @SerializedName("ementa")
    val description: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("numero")
    val number: Int,
    @SerializedName("uri")
    val uri: String
)