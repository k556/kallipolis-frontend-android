package com.kallipolis.https

import com.kallipolis.model.*
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.Body

interface FavoritesService {
    @POST("usuario/{id}/deputado_favorito")
    suspend fun addNewFavoriteDeputy(@Path("id") userId: Int,
                                     @Body favorite: FavoriteDeputy):
            Response<FavoriteDeputyResponse>

    @GET("usuario/{id}/deputado_favorito")
    suspend fun getAllFavoriteDeputies(@Path("id") userId: Int):
            Response<AllFavoriteDeputiesResponse>

    @HTTP(method = "DELETE", path = "usuario/{id}/deputado_favorito", hasBody = true)
    suspend fun deleteFavoriteDeputy(@Path("id") userId: Int,
                                     @Body favorite: FavoriteDeputyItem)

    @POST("usuario/{id}/partido_favorito")
    suspend fun addNewFavoriteParty(@Path("id") userId: Int,
                                    @Body favorite: FavoriteParty):
            Response<FavoritePartyResponse>

    @GET("usuario/{id}/partido_favorito")
    suspend fun getAllFavoriteParties(@Path("id") userId: Int):
            Response<FavoritePartyResponse>

    @HTTP(method = "DELETE", path = "usuario/{id}/partido_favorito", hasBody = true)
    suspend fun deleteFavoriteParty(@Path("id")userId: Int,
                                    @Body favorite: FavoritePartyItem)

}