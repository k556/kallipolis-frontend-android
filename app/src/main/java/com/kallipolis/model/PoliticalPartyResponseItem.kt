package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class PoliticalPartyResponseItem(
    @SerializedName("id")
    val id: Int,
    @SerializedName("idLegislatura")
    val idLegislature: String,
    @SerializedName("lider")
    val leader: Int,
    @SerializedName("nome")
    val name: String,
    @SerializedName("numeroEleitoral")
    val electoralNumber: Any,
    @SerializedName("sigla")
    val partyInitials: String,
    @SerializedName("situacao")
    val status: String,
    @SerializedName("totalMembros")
    val totalMembers: Int,
    @SerializedName("totalPosse")
    val totalMandate: Int,
    @SerializedName("uri")
    val uri: String,
    @SerializedName("uriMembros")
    val uriMembers: String,
    @SerializedName("urlFacebook")
    val urlFacebook: String?,
    @SerializedName("urlLogo")
    val urlLogo: String,
    @SerializedName("urlWebsite")
    val urlWebsite: String?,
    @SerializedName("favorito")
    var isFavorite: Boolean
)