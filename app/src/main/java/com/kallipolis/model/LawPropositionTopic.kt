package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class LawPropositionTopic(
    @SerializedName("favorite")
    val favorite: Boolean,
    @SerializedName("id")
    val id: Int,
    @SerializedName("nome")
    val name: String,
    var isSelected: Boolean = false
)