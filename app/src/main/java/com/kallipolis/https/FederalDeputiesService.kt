package com.kallipolis.https

import com.kallipolis.model.DeputiesPageResponse
import com.kallipolis.model.DeputyAllExpensesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface FederalDeputiesService {
    @GET("usuario/{userId}/deputado_favorito/all")
    suspend fun fetchAllFederalDeputies(@Path("userId") userId: Int,
                                        @Query("page") page: Int,
                                        @Query("size") size: Int):
                                        Response<DeputiesPageResponse>

    @GET("deputado/{deputyId}/despesas")
    suspend fun fetchDeputyExpenses(@Path("deputyId") deputyId: Int):
                                        Response<DeputyAllExpensesResponse>
}