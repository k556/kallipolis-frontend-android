package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class LawPropositionThemeDetails(
    @SerializedName("codTema")
    val themeId: Int,
    @SerializedName("relevancia")
    val relevance: Int,
    @SerializedName("tema")
    val themeDescription: String
)