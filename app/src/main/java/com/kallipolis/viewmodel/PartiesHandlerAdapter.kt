package com.kallipolis.viewmodel

interface PartiesHandlerAdapter {
    fun addNewFavoriteParty(userId: Int, partyId: Int)
    fun deleteFavoriteParty(userId: Int, partyId: Int)
    fun showPartyDetails(partyId: Int)
}