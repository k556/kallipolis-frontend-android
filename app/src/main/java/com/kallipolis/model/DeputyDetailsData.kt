package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class DeputyDetailsData(
    @SerializedName("cpf")
    val cpf: String,
    @SerializedName("dataFalecimento")
    val deathDate: Any,
    @SerializedName("dataNascimento")
    val birthDate: String,
    @SerializedName("escolaridade")
    val scholarityLevel: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("municipioNascimento")
    val birthCity: String,
    @SerializedName("nomeCivil")
    val civilName: String,
    @SerializedName("redeSocial")
    val socialNetworks: List<Any>,
    @SerializedName("sexo")
    val gender: String,
    @SerializedName("ufNascimento")
    val birthState: String,
    @SerializedName("ultimoStatus")
    val lastSatatusData: LastStatusData,
    @SerializedName("uri")
    val uri: String,
    @SerializedName("urlWebsite")
    val urlWebsite: Any
)