package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class FavoritePartyResponseItem(
    @SerializedName("id")
    val id: Int,
    @SerializedName("idPartido")
    val partyId: Int,
    @SerializedName("idUsuario")
    val userId: Int,
    @SerializedName("partido")
    val party: PoliticalPartyResponseItem
)