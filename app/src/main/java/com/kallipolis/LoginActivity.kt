package com.kallipolis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.kallipolis.databinding.ActivityLoginBinding
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.LoginActivityViewModel

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private val model: LoginActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        initViews()
        initObservers()
    }

    private fun initViews() {
        actionBar?.hide()
        supportActionBar?.hide()
        val window = this.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = this.resources.getColor(R.color.status_bar_color)
        binding.apply {
            loginBtnSendEmail.setOnClickListener(View.OnClickListener {
                model.setUser(loginEtName.text.toString(), loginEtEmail.text.toString())
            })

            val inputWatcher = LoginInputInfoWatcher(binding, model)
            binding.loginEtName.addTextChangedListener(inputWatcher)
            binding.loginEtEmail.addTextChangedListener(inputWatcher)
        }
    }

    private fun initObservers() {
        model.currentUser.observe(this, {
            if (it == null) {
                val errorLoginMessage = getString(R.string.login_error)
                Toast.makeText(this, errorLoginMessage, Toast.LENGTH_LONG).show()
            } else {
                val successLoginMessage = getString(R.string.login_success, it.nome)
                Toast.makeText(this, successLoginMessage, Toast.LENGTH_LONG).show()
                saveCurrentUserId(it.id)
                goToHome()
            }
        })

        model.isValidUserInfo.observe(this, {
            if (it) {
                binding.loginBtnSendEmail.isEnabled = true
                binding.loginBtnSendEmail.setBackgroundColor(resources.getColor(R.color.kallipolis_primary))
            } else {
                binding.loginBtnSendEmail.isEnabled = false
                binding.loginBtnSendEmail.setBackgroundColor(resources.getColor(R.color.login_button))
            }
        })

    }

    private fun goToHome() {
        val intentToHome = Intent(this, HomeActivity::class.java)
        startActivity(intentToHome)
    }

    private fun saveCurrentUserId(userId: Int) {
        StorageUtils.storeUserId(this, userId)
    }

    override fun onBackPressed() {
        /*DO NOTHING*/
    }

    private class LoginInputInfoWatcher(private val binding: ActivityLoginBinding,
                                        private val loginActivityViewModel: LoginActivityViewModel): TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            /*DO NOTHING*/
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            /*DO NOTHING*/
        }

        override fun afterTextChanged(p0: Editable?) {
            val userName = binding.loginEtName.text
            val userEmail = binding.loginEtEmail.text
            loginActivityViewModel.validateUserLoginInfo(userName.toString(), userEmail.toString())
        }

    }
}