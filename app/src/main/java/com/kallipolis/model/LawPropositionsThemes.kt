package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class LawPropositionsThemes(
    @SerializedName("topicos")
    val lawPropositionTopics: List<LawPropositionTopic>
)