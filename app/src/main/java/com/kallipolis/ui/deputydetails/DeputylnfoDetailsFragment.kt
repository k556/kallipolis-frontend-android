package com.kallipolis.ui.deputydetails

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide

import com.kallipolis.databinding.FragmentDeputyDetailsInfosBinding
import com.kallipolis.model.DeputyDetailsData
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.DeputylnfoDetailsViewModel
import com.kallipolis.utils.StringUtils.Companion.capitalizeFirstLetterOfEachWord
import com.kallipolis.utils.StringUtils.Companion.parseDateToBrazilianFormat


class DeputylnfoDetailsFragment : Fragment() {

    private lateinit var viewModel: DeputylnfoDetailsViewModel
    private var _binding: FragmentDeputyDetailsInfosBinding? = null
    private val binding get() = _binding!!
    private var deputId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(DeputylnfoDetailsViewModel::class.java)
        _binding = FragmentDeputyDetailsInfosBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initFragmentArguments()
        initObservers()
        fetchViewData()
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initFragmentArguments() {
        this.deputId = arguments?.getInt(StorageUtils.DEPUTY_ID_PARAMETER)!!
    }

    private fun initObservers() {
        viewModel.selectedDeputyDetails.observe(viewLifecycleOwner, {
            if (it != null) {
                initViewData(it.details)
            }
        })

        viewModel.isFavoriteDeputy.observe(viewLifecycleOwner, {

        })
    }

    private fun initViewData(deputyInfo: DeputyDetailsData) {
        val civilName = capitalizeFirstLetterOfEachWord(deputyInfo.civilName.lowercase())
        val birthDateStr = parseDateToBrazilianFormat(deputyInfo.birthDate)
        val birthPlace = "${deputyInfo.birthCity} ⋅ ${deputyInfo.birthState}"

        Glide.with(requireContext())
            .load(deputyInfo.lastSatatusData.profilePictureUrl)
            .into(binding.popupDeputyProfilePicture)

        binding.popupDeputyName.text = deputyInfo.lastSatatusData.name
        binding.popupCivilName.text = civilName
        binding.popupBirthPlace.text = birthPlace
        binding.popupBirthDate.text = birthDateStr
        binding.popupDeputyCpf.text = deputyInfo.cpf
        binding.popupDeputyScholarityLevel.text = deputyInfo.scholarityLevel
        binding.popupPartyInitials.text = deputyInfo.lastSatatusData.partyInitials
        binding.popupPartyStateInitials.text = deputyInfo.lastSatatusData.stateInitials
        binding.popupDeputyEmail.text = deputyInfo.lastSatatusData.email
        binding.popupDeputyPhone.text = deputyInfo.lastSatatusData.cabinetInfo.phoneNumber
        binding.popupDeputyBuilding.text = deputyInfo.lastSatatusData.cabinetInfo.building
        binding.popupDeputyRoom.text = deputyInfo.lastSatatusData.cabinetInfo.room
        binding.popupDeputyFloor.text = deputyInfo.lastSatatusData.cabinetInfo.floor
    }

    private fun fetchViewData() {
       viewModel.showDeputyDetails(this.deputId)
    }

}