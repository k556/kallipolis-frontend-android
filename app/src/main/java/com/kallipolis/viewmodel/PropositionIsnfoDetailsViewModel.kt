package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.LawPropositionDetailsResponse
import com.kallipolis.repository.LawPropositionsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PropositionIsnfoDetailsViewModel : ViewModel() {
    private val _propositionDetails = MutableLiveData<LawPropositionDetailsResponse?>()
    val propositionDetails: LiveData<LawPropositionDetailsResponse?> = _propositionDetails

    private val lawPropositionsRepository = LawPropositionsRepository()

    init {
        _propositionDetails.value = null
    }

    fun getPropositionDetails(propositionId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = lawPropositionsRepository.getLawPropositionDetails(propositionId)
            if (response.isSuccessful) {
                val propositionDetails = response.body()
                if (propositionDetails != null)
                    withContext(Dispatchers.Main) {
                        _propositionDetails.value = propositionDetails
                    }
            }
        }
    }
}