package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.FavoritePartyResponse
import com.kallipolis.model.PoliticalPartyDetails
import com.kallipolis.model.PoliticalPartyResponseItem
import com.kallipolis.repository.FavoritesRepository
import com.kallipolis.repository.PoliticalPartiesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavoritePartiesViewModel: ViewModel(), PartiesHandlerAdapter {
    private var _allFavoriteParties = MutableLiveData<MutableList<PoliticalPartyResponseItem>>()
    val allFavoriteParties: LiveData<MutableList<PoliticalPartyResponseItem>> = _allFavoriteParties

    private val _selectedPartyDetails = MutableLiveData<PoliticalPartyDetails>()
    val selectedPartyDetails: LiveData<PoliticalPartyDetails> = _selectedPartyDetails

    private val favoritesRepository = FavoritesRepository()
    private val partiesRepository = PoliticalPartiesRepository()

    init {
        _allFavoriteParties.value = mutableListOf()
    }

    fun getAllFavoriteParties(userId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val allFavoriteParties: FavoritePartyResponse? = favoritesRepository.getAllFavoriteParties(userId)
            if (!allFavoriteParties!!.isEmpty()) {
                withContext(Dispatchers.Main) {
                    val favoriteParties: MutableList<PoliticalPartyResponseItem> =
                        buildListOfFavoriteParties(allFavoriteParties)
                    withContext(Dispatchers.Main) {
                        _allFavoriteParties.value = favoriteParties
                    }
                }
            } else {
                withContext(Dispatchers.Main) {
                    _allFavoriteParties.value = mutableListOf()
                }
            }
        }
    }

    private fun buildListOfFavoriteParties(allFavoriteParties: FavoritePartyResponse):
            MutableList<PoliticalPartyResponseItem> {
        val allFavoritePartiesList = mutableListOf<PoliticalPartyResponseItem>()

        for (favorite in allFavoriteParties) {
            val currentFavoriteParty: PoliticalPartyResponseItem = favorite.party
            currentFavoriteParty.isFavorite = true
            allFavoritePartiesList.add(currentFavoriteParty)
        }

        return allFavoritePartiesList
    }

    override fun addNewFavoriteParty(userId: Int, partyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            favoritesRepository.addNewFavoriteParty(userId, partyId)
            getAllFavoriteParties(userId)
        }
    }

    override fun deleteFavoriteParty(userId: Int, partyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            favoritesRepository.deleteFavoriteParty(userId, partyId)
            getAllFavoriteParties(userId)
        }
    }

    override fun showPartyDetails(partyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = partiesRepository.getPoliticalPartyDetails(partyId).body()!!
            withContext(Dispatchers.Main) {
                _selectedPartyDetails.value = response
            }
        }
    }

}