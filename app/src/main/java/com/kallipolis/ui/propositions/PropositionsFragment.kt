package com.kallipolis.ui.propositions

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kallipolis.PropositionsFilterActivity
import com.kallipolis.R
import com.kallipolis.databinding.FragmentPropositionsBinding
import com.kallipolis.model.LawPropositionFilter
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.LawPropositionViewModel

class PropositionsFragment : Fragment() {

    private lateinit var lawsViewModel: LawPropositionViewModel
    private var _binding: FragmentPropositionsBinding? = null
    private var propositionItemCardViewAdapter: PropositionItemCardViewAdapter? = null
    private var propositionFilters: LawPropositionFilter? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        lawsViewModel = ViewModelProvider(this).get(LawPropositionViewModel::class.java)
        _binding = FragmentPropositionsBinding.inflate(inflater, container,false)
        val root: View = binding.root

        loadPropositionFilters()
        initObservers()
        initViewData()
        initViews()
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initObservers() {
        lawsViewModel.listOfLaws.observe(viewLifecycleOwner, {
            val propositions = ArrayList(it)
            if (propositionItemCardViewAdapter == null) {
                propositionItemCardViewAdapter = PropositionItemCardViewAdapter(propositions, requireContext())
                binding.propositionsRecyclerView.adapter = propositionItemCardViewAdapter
            } else {
                propositionItemCardViewAdapter!!.updateData(propositions)
                if (lawsViewModel.isFirstPage()) {
                    binding.propositionsRecyclerView.layoutManager!!.scrollToPosition(0)
                }
            }

            binding.propositionNumberResultsSearch.visibility = View.VISIBLE
            binding.propositionNumberResultsSearch.text = "${propositions.size.toString()} resultados"
        })

        lawsViewModel.propositionsFilter.observe(viewLifecycleOwner, {
            if (it == null) {
                lawsViewModel.getAllLaws()
            } else {
                lawsViewModel.getLawPropositionsWithFilters()
            }
        })

        lawsViewModel.shouldDisplayLoading.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressPropositions.visibility = View.VISIBLE
            } else {
                binding.progressPropositions.visibility = View.GONE
            }
        })

    }

    private fun initViewData() {
        if (shouldApplyPropositionFilters()) {
            lawsViewModel.getLawPropositionsWithFilters()
        } else {
            lawsViewModel.getAllLaws()
        }
    }

    private fun initViews() {
        binding.propositionsRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.propositionsSearch.setStartIconDrawable(R.drawable.ic_search_field)
        binding.propositionNumberResultsSearch.visibility = View.GONE
        binding.propositionsFilter.setOnClickListener {
            val intent = Intent(requireContext(), PropositionsFilterActivity::class.java)
            startActivity(intent)
        }
        binding.propositionsRecyclerView.addOnScrollListener(OnPropositionsViewScrollListener())
    }

    private fun retrieveUserId():Int {
        return StorageUtils.retrieveUserId(requireContext())
    }

    private fun loadPropositionFilters() {
        val hasPropositionsFilterApplyExtra = requireActivity().intent.hasExtra(StorageUtils.PROPOSITIONS_FILTER_APPLY)
        if (hasPropositionsFilterApplyExtra) {
            val bundleFilter = requireActivity().intent.getBundleExtra(StorageUtils.PROPOSITIONS_FILTER_APPLY)
            this.propositionFilters = bundleFilter!!.getParcelable(StorageUtils.PROPOSITIONS_FILTER_APPLY)
            lawsViewModel.setNewFilter(this.propositionFilters!!)
            requireActivity().intent.removeExtra(StorageUtils.PROPOSITIONS_FILTER_APPLY)
        }
    }

    private fun shouldApplyPropositionFilters() = this.propositionFilters != null

    private inner class OnPropositionsViewScrollListener : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (isScrollingDown(dy) && hasReachedBottom(recyclerView)) {
                if (lawsViewModel.hasFilterApplied()) {
                    lawsViewModel.getLawPropositionsWithFilters()
                } else {
                    lawsViewModel.getAllLaws()
                }
            }
        }

        private fun hasReachedBottom(recyclerView: RecyclerView): Boolean {
            return !recyclerView.canScrollVertically(1)
        }

        private fun isScrollingDown(dy: Int): Boolean = dy > 0
    }
}