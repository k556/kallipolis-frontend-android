package com.kallipolis.https

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ParliamentRetrofitInstance private constructor() {
    companion object {
        private const val REQUEST_TIMEOUT: Long = 30

        private const val PARLIAMENT_URL: String = "https://dadosabertos.camara.leg.br/api/v2/"

        private val interceptor = HttpLoggingInterceptor().apply {
            this.level = HttpLoggingInterceptor.Level.BODY
        }

        private val client = OkHttpClient.Builder().apply {
            this.addInterceptor(interceptor)
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
        }.build()

        fun getParliamentRetrofitSingletonInstance(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(PARLIAMENT_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
        }
    }
}