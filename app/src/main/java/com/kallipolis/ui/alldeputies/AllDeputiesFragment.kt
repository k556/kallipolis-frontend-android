package com.kallipolis.ui.alldeputies

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kallipolis.databinding.FragmentAllDeputiesBinding
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.AllDeputiesViewModel

class AllDeputiesFragment : Fragment() {

    private lateinit var allDeputiesViewModel: AllDeputiesViewModel
    private var _binding: FragmentAllDeputiesBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var deputyItemCardViewAdapter: DeputyItemCardViewAdapter? = null

    private var currentUserId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        allDeputiesViewModel =
            ViewModelProvider(this).get(AllDeputiesViewModel::class.java)

        _binding = FragmentAllDeputiesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        initObservers()
        initViewData()
        initViews()
        val currentUserId = retrieveUserId()
        Log.d("DEBUG", "current retrieved user id: $currentUserId")
        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initViews() {
        binding.allDeputiesFrameLayout.foreground.alpha = 0
        binding.allDeputiesRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.allDeputiesRecyclerView.addOnScrollListener(OnScrollListener(allDeputiesViewModel, currentUserId))
    }

    private fun initObservers() {
        allDeputiesViewModel.allDeputies.observe(viewLifecycleOwner, {
            val deputies = ArrayList(it)
            if (deputyItemCardViewAdapter == null) {
                deputyItemCardViewAdapter = DeputyItemCardViewAdapter(deputies, allDeputiesViewModel, currentUserId)
                binding.allDeputiesRecyclerView.adapter = deputyItemCardViewAdapter
            } else {
                deputyItemCardViewAdapter!!.updateData(deputies)
            }
        })

        allDeputiesViewModel.shouldDisplayLoading.observe(viewLifecycleOwner, {
            if (it) {
                binding.progressDeputies.visibility = View.VISIBLE
            } else {
                binding.progressDeputies.visibility = View.GONE
            }
        })

        allDeputiesViewModel.newFavoriteDeputyPosition.observe(viewLifecycleOwner, {
            deputyItemCardViewAdapter!!.setNewFavorite(it)
        })

        allDeputiesViewModel.deletedFavoriteDeputyPosition.observe(viewLifecycleOwner, {
            deputyItemCardViewAdapter!!.unsetFavorite(it)
        })
    }

    private fun initViewData() {
        currentUserId = retrieveUserId()
        allDeputiesViewModel.getAllDeputies(currentUserId)
    }

    class OnScrollListener(val allDeputiesViewModel: AllDeputiesViewModel,
                           val currentUserId: Int):
        RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            if (isScrollingDown(dy) && hasReachedBottom(recyclerView)) {
                    allDeputiesViewModel.getAllDeputies(currentUserId)
            }
        }

        private fun hasReachedBottom(recyclerView: RecyclerView): Boolean {
            return !recyclerView.canScrollVertically(1)
        }

        private fun isScrollingDown(dy: Int): Boolean = dy > 0
    }

    private fun retrieveUserId():Int {
        return StorageUtils.retrieveUserId(requireContext())
    }
}