package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.LawPropositionFilter
import com.kallipolis.model.LawPropositionTopic
import com.kallipolis.model.LawPropositionsResponseItem
import com.kallipolis.repository.LawPropositionsRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LawPropositionViewModel : ViewModel() {

    private var currentPageIndex = 1

    private val _listOfLaws = MutableLiveData<MutableList<LawPropositionsResponseItem>>()
    val listOfLaws: LiveData<MutableList<LawPropositionsResponseItem>> = _listOfLaws

    private val _listOfPropositionTopics = MutableLiveData<MutableList<LawPropositionTopic>>()
    val listOfPropositionTopics: LiveData<MutableList<LawPropositionTopic>> = _listOfPropositionTopics

    private val _propositionsFilter = MutableLiveData<LawPropositionFilter?>()
    val propositionsFilter: LiveData<LawPropositionFilter?> = _propositionsFilter

    private val _shouldDisplayLoading = MutableLiveData<Boolean>()
    val shouldDisplayLoading: LiveData<Boolean> = _shouldDisplayLoading

    private val lawPropositionsRepository = LawPropositionsRepository()

    init {
        _shouldDisplayLoading.value = false
        _propositionsFilter.value = null
        _listOfLaws.value = mutableListOf()
    }

    fun getAllLaws() {
        toggleLoadingVisibility()
        CoroutineScope(Dispatchers.IO).launch {
            val response = lawPropositionsRepository.getAllLawPropositions(currentPageIndex)
            if (response.isSuccessful) {
               withContext(Dispatchers.Main) {
                   notifyViews(response.body()!!)
               }
            }
            increasePageIndex()
        }
    }

    fun getAllPropositionTopics(userId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = lawPropositionsRepository.getAllLawPropositionThemes(userId)
            if (response.isSuccessful) {
                val propositions = response.body()!!.lawPropositionTopics.toMutableList()
                withContext(Dispatchers.Main) {
                    _listOfPropositionTopics.value = propositions
                }
            }
        }
    }

    fun getLawPropositionsWithFilters() {
        toggleLoadingVisibility()
        CoroutineScope(Dispatchers.IO).launch {
            val response = lawPropositionsRepository
                .getLawPropositionsWithFilters(_propositionsFilter.value!!, currentPageIndex)
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    notifyViews(response.body()!!)
                }

                increasePageIndex()
            }
        }
    }

    fun notifyViews(listFetchedPropositions: MutableList<LawPropositionsResponseItem>) {
        var oldPropositions: MutableList<LawPropositionsResponseItem> = mutableListOf()
        if (!isFirstPage())
            oldPropositions = _listOfLaws.value!!
        val newLawPropositions = oldPropositions + listFetchedPropositions
        _listOfLaws.value = newLawPropositions.toMutableList()
        toggleLoadingVisibility()
    }

    fun cleanAllFilters() {
        _propositionsFilter.value = null
    }

    fun setNewFilter(filter: LawPropositionFilter) {
        cleanPageIndex()
        _propositionsFilter.value = filter
    }

    private fun increasePageIndex() = currentPageIndex++

    private fun toggleLoadingVisibility() { _shouldDisplayLoading.value = !_shouldDisplayLoading.value!! }

    private fun cleanPageIndex() {
        currentPageIndex = 1
    }

    fun isFirstPage(): Boolean = currentPageIndex == 1

    fun hasFilterApplied(): Boolean = _propositionsFilter.value != null
}