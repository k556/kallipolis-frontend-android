package com.kallipolis

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.google.android.material.shape.MaterialShapeDrawable
import com.kallipolis.databinding.ActivityDeputyDetailsBinding
import com.kallipolis.ui.deputydetails.DeputyPropositionsWebviewFragment
import com.kallipolis.ui.deputydetails.DeputyExpensesFragment
import com.kallipolis.ui.deputydetails.DeputyVotingInfoWebviewFragment
import com.kallipolis.ui.deputydetails.DeputylnfoDetailsFragment
import com.kallipolis.utils.StorageUtils

class DeputyDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDeputyDetailsBinding
    private var deputId: Int = 0
    private var deputyName: String = ""

    companion object {
        const val NAME_START_IDX = 0
        const val MAX_TOOLBAR_TITLE_LENGTH = 30
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_deputy_details)
        val deputyDetailsSlidePagerAdapter = DeputyDetailsSlidePagerAdapter(supportFragmentManager)
        binding.deputyDetailsViewPager.adapter = deputyDetailsSlidePagerAdapter
        binding.tabsDeputyDetails.setupWithViewPager(binding.deputyDetailsViewPager)
        initAcitivityArguments()
        initViews()
    }

    private fun initViews() {
        actionBar?.hide()
        supportActionBar?.hide()
        binding.deputyDetailsToolbarTitle.text = deputyName
        val window = this.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = this.resources.getColor(R.color.status_bar_color)
        val materialToolbarShape = binding.deputyDetailsToolbar.background as MaterialShapeDrawable
        materialToolbarShape.elevation = HomeActivity.HOME_NAV_ELEMENTS_ELEVATION
        materialToolbarShape.shadowCompatibilityMode =
            MaterialShapeDrawable.SHADOW_COMPAT_MODE_ALWAYS
        materialToolbarShape.shapeAppearanceModel = materialToolbarShape.shapeAppearanceModel
            .toBuilder()
            .setBottomLeftCornerSize(HomeActivity.TOOLBAR_BORDER_RADIUS)
            .setBottomRightCornerSize(HomeActivity.TOOLBAR_BORDER_RADIUS)
            .build()
    }

    private inner class DeputyDetailsSlidePagerAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm) {
        private val numPages = 4

        override fun getCount(): Int {
            return numPages
        }

        override fun getItem(position: Int): Fragment {
            val args = Bundle()
            args.putInt(StorageUtils.DEPUTY_ID_PARAMETER, deputId)

            return if (position == 0) {
                val deputylnfoDetailsFragment = DeputylnfoDetailsFragment()
                deputylnfoDetailsFragment.arguments = args
                deputylnfoDetailsFragment
            } else if (position == 1) {
                val deputyVotingInfoWebviewFragment = DeputyVotingInfoWebviewFragment()
                deputyVotingInfoWebviewFragment.arguments = args
                deputyVotingInfoWebviewFragment
            } else  if (position == 2) {
                val deputyExpensesFragment = DeputyExpensesFragment()
                deputyExpensesFragment.arguments = args
                deputyExpensesFragment
            } else {
                val deputyPropositionsWebviewFragment = DeputyPropositionsWebviewFragment()
                deputyPropositionsWebviewFragment.arguments = args
                deputyPropositionsWebviewFragment
            }
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> "Detalhes"
                1 -> "Votações"
                2 -> "Gastos"
                else -> "Propostas"
            }
        }
    }

    private fun initAcitivityArguments() {
        this.deputId = intent.getIntExtra(StorageUtils.DEPUTY_ID_PARAMETER, 0)
        this.deputyName = intent.getStringExtra(StorageUtils.DEPUTY_NAME_PARAMETER)!!
        if (this.deputyName.length > MAX_TOOLBAR_TITLE_LENGTH)
            this.deputyName = this.deputyName.substring(NAME_START_IDX, MAX_TOOLBAR_TITLE_LENGTH - 1)
    }

}