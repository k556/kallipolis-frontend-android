package com.kallipolis

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Rule
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import org.junit.Ignore

@Ignore
@RunWith(AndroidJUnit4::class)
class SplashScreenInstrumentedTest {
    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Test
    fun allViewsOnSplashScreenAreDisplayed() {
        onView(withId(R.id.splash_screen_icon))
            .check(matches(isDisplayed()))

        Thread.sleep(1000)

        onView(withId(R.id.splash_screen_title))
            .check(matches(isDisplayed()))

        Thread.sleep(1000)

        onView(withId(R.id.splash_screen_progress))
            .check(matches(isDisplayed()))

        Thread.sleep(1000)
    }
}