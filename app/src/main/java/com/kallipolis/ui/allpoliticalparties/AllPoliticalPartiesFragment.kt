package com.kallipolis.ui.allpoliticalparties

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.kallipolis.databinding.FragmentAllPoliticalPartiesBinding
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.AllPoliticalPartiesViewModel

class AllPoliticalPartiesFragment : Fragment() {

    private lateinit var allPoliticalPartiesViewModel: AllPoliticalPartiesViewModel
    private var _binding: FragmentAllPoliticalPartiesBinding? = null

    private val binding get() = _binding!!

    private var politicalPartyCardViewAdapter: PoliticalPartyCardViewAdapter? = null

    private var userId: Int = StorageUtils.INVALID_USER_ID

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        allPoliticalPartiesViewModel =
            ViewModelProvider(this).get(AllPoliticalPartiesViewModel::class.java)

        _binding = FragmentAllPoliticalPartiesBinding.inflate(inflater, container, false)
        val root: View = binding.root


        initObservers()
        initViews()
        retrieveUserId()
        initViewData()
        return root
    }

    private fun initObservers() {
        allPoliticalPartiesViewModel.allPoliticalPartiesVM.observe(viewLifecycleOwner, {
            val parties = ArrayList(it!!)
            politicalPartyCardViewAdapter = PoliticalPartyCardViewAdapter(parties, allPoliticalPartiesViewModel, userId)
            binding.allPartiesRecyclerView.adapter = politicalPartyCardViewAdapter
        })

        allPoliticalPartiesViewModel.newFavoritePoliticalPartyPosition.observe(viewLifecycleOwner, {
            politicalPartyCardViewAdapter!!.setNewFavorite(it)
        })

        allPoliticalPartiesViewModel.deletedFavoritePartyPosition.observe(viewLifecycleOwner, {
            politicalPartyCardViewAdapter!!.unsetFavorite(it)
        })

        allPoliticalPartiesViewModel.selectedPartyDetails.observe(viewLifecycleOwner, {
            PartyDetailsPopup().showPartyDetailsPopup(requireView(),
                binding.allPartiesFrameLayout,
                it,
                requireContext())
        })
    }

    private fun initViews() {
        binding.allPartiesFrameLayout.foreground.alpha = 0
        binding.allPartiesRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    private fun initViewData() {
        allPoliticalPartiesViewModel.getAllPoliticalParties(this.userId)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun retrieveUserId() {
        this.userId =  StorageUtils.retrieveUserId(requireContext())
    }
}