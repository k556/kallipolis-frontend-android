package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.DeputyDetails
import com.kallipolis.repository.AllDeputiesRepository
import com.kallipolis.repository.FavoritesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DeputylnfoDetailsViewModel : ViewModel(), DeputiesHandlerAdapter {
    private val _selectedDeputyDetails = MutableLiveData<DeputyDetails?>()
    val selectedDeputyDetails: LiveData<DeputyDetails?> = _selectedDeputyDetails

    private val _isFavoriteDeputy = MutableLiveData<Boolean>()
    val isFavoriteDeputy: LiveData<Boolean> = _isFavoriteDeputy

    private val allDeputiesRepository = AllDeputiesRepository()
    private val favoritesRepository = FavoritesRepository()

    init {
        _selectedDeputyDetails.value = null
    }

    override fun addNewFavoriteDeputy(userId: Int, deputyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = favoritesRepository.addNewFavoriteDeputy(userId, deputyId)
            if (response != null)
                setNewFavorite()
        }
    }

    override fun showDeputyDetails(deputyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = allDeputiesRepository.getDeputyDetails(deputyId)
            val deputyDetails = response.body()!!
            withContext(Dispatchers.Main) {
                _selectedDeputyDetails.value = deputyDetails
            }
        }
    }

    override fun deleteFavoriteDeputy(userId: Int, deputyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            favoritesRepository.deleteFavoriteDeputy(userId, deputyId)
            setDeletedFavorite()
        }
    }

    private suspend fun setNewFavorite() {
        withContext(Dispatchers.Main) {
            _isFavoriteDeputy.value = true
        }
    }

    private suspend fun setDeletedFavorite() {
        withContext(Dispatchers.Main) {
            _isFavoriteDeputy.value = false
        }
    }
}