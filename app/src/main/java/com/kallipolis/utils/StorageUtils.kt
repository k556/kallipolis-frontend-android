package com.kallipolis.utils

import android.content.Context

class StorageUtils private constructor() {
    companion object SessionUtils {
        const val SHARED_PREFS_NAME = "SETTINGS"
        const val SHARED_PREFS_USER_ID: String = "USER_ID"
        const val INVALID_USER_ID = -1
        const val DEPUTY_ID_PARAMETER: String = "DEPUTY_ID_PARAMETER"
        const val DEPUTY_NAME_PARAMETER: String = "DEPUTY_NAME_PARAMETER"
        const val PROPOSITION_ID_PARAMETER: String = "PROPOSITION_ID_PARAMETER"
        const val PROPOSITION_TITLE_PARAMETER: String = "PROPOSITION_TITLE_PARAMETER"
        const val SHOULD_NAVIGATE_PROPOSITIONS = "SHOULD_NAVIGATE_PROPOSITIONS"
        const val PROPOSITIONS_FILTER_APPLY = "PROPOSITIONS_FILTER_APPLY"

        fun storeUserId(context: Context, userId: Int) {
            val sharedPrefs =
                context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE) ?: return

            with(sharedPrefs.edit()) {
                putInt(SHARED_PREFS_USER_ID, userId)
                commit()
            }
        }

        fun retrieveUserId(context: Context): Int {
            val sharedPrefs =
                context.getSharedPreferences(SHARED_PREFS_NAME, Context.MODE_PRIVATE)
                    ?: return INVALID_USER_ID

            return sharedPrefs.getInt(SHARED_PREFS_USER_ID, INVALID_USER_ID)
        }

    }
}