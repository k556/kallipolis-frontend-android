package com.kallipolis.https

import com.kallipolis.model.DeputyDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface DetailsService {
    @GET("deputados/{id}")
    suspend fun fetchDeputyDetails(@Path("id") deputyId: Int): Response<DeputyDetails>
}