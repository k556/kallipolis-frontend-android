package com.kallipolis

import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.MaterialShapeDrawable.SHADOW_COMPAT_MODE_ALWAYS
import com.kallipolis.databinding.ActivityHomeBinding
import com.kallipolis.utils.StorageUtils

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHomeBinding

    companion object BorderRadius {
       const val BOTTOM_NAV_BORDER_RADIUS: Float = 40f
       const val TOOLBAR_BORDER_RADIUS: Float = 30f
       const val HOME_NAV_ELEMENTS_ELEVATION: Float = 20f
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_home)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_deputies, R.id.nav_parties, R.id.nav_favorites, R.id.nav_propositions
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        val itemNavBottomBarListener = NavBottomBarClickListener(binding, navController, supportFragmentManager)
        binding.navView.setOnNavigationItemSelectedListener(itemNavBottomBarListener)
        initViews()

        if (intent.hasExtra(StorageUtils.SHOULD_NAVIGATE_PROPOSITIONS)) {
            navController.navigate(R.id.nav_propositions)
        }
    }

    private fun initViews() {
        actionBar?.hide()
        supportActionBar?.hide()
        val window = this.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = this.resources.getColor(R.color.status_bar_color)

        val shapeDrawable : MaterialShapeDrawable= binding.navView.background as MaterialShapeDrawable
        shapeDrawable.elevation = HOME_NAV_ELEMENTS_ELEVATION
        shapeDrawable.shadowCompatibilityMode = SHADOW_COMPAT_MODE_ALWAYS
        shapeDrawable.shapeAppearanceModel = shapeDrawable.shapeAppearanceModel
            .toBuilder()
            .setTopLeftCornerSize(BOTTOM_NAV_BORDER_RADIUS)
            .setTopRightCornerSize(BOTTOM_NAV_BORDER_RADIUS)
            .build()

        val materialToolbarShape = binding.homeToolbar.background as MaterialShapeDrawable
        materialToolbarShape.elevation = HOME_NAV_ELEMENTS_ELEVATION
        materialToolbarShape.shadowCompatibilityMode = SHADOW_COMPAT_MODE_ALWAYS
        materialToolbarShape.shapeAppearanceModel = materialToolbarShape.shapeAppearanceModel
            .toBuilder()
            .setBottomLeftCornerSize(TOOLBAR_BORDER_RADIUS)
            .setBottomRightCornerSize(TOOLBAR_BORDER_RADIUS)
            .build()
    }

    private class  NavBottomBarClickListener(val binding: ActivityHomeBinding,
                                             val navController: NavController,
                                            val fragmentManager: FragmentManager):
        BottomNavigationView.OnNavigationItemSelectedListener {
        override fun onNavigationItemSelected(item: MenuItem): Boolean {
            val fragmentTitle = item.title
            binding.homeToolbarTitle.text = fragmentTitle
            // navController.navigate(item.itemId)
            return item.onNavDestinationSelected(navController)
        }
    }
}