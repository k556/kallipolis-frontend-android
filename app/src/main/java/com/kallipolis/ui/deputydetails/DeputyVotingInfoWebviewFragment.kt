package com.kallipolis.ui.deputydetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kallipolis.databinding.FragmentDeputyVotingInfoWebviewBinding
import com.kallipolis.utils.StorageUtils

class DeputyVotingInfoWebviewFragment : Fragment() {

    private var _binding: FragmentDeputyVotingInfoWebviewBinding? = null
    private val binding get() = _binding!!
    private var deputId: Int = 0

    private val baseWebviewUrl =
        "http://kallipolis-webviews.s3-website-us-east-1.amazonaws.com/votacao-prop-deputado/"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDeputyVotingInfoWebviewBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initFragmentArguments()
        loadWebviewVotingInfoData()
        return root
    }

    private fun initFragmentArguments() {
        this.deputId = arguments?.getInt(StorageUtils.DEPUTY_ID_PARAMETER)!!
    }

    private fun loadWebviewVotingInfoData() {
        val url = "${baseWebviewUrl}${this.deputId}"
        binding.deputyDetailsVotingInfo.settings.builtInZoomControls = false
        binding.deputyDetailsVotingInfo.settings.javaScriptEnabled = true
        binding.deputyDetailsVotingInfo.settings.domStorageEnabled = true
        binding.deputyDetailsVotingInfo.loadUrl(url)
    }
}