package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.DeputyAllExpensesResponse
import com.kallipolis.repository.AllDeputiesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DeputyExpensesViewModel : ViewModel() {
    private val _expenses = MutableLiveData<DeputyAllExpensesResponse>()
    val expenses: LiveData<DeputyAllExpensesResponse> = _expenses

    val deputyRepository = AllDeputiesRepository()

    fun getAllDeputyExpenses(deputyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = deputyRepository.getDeputyExpenses(deputyId)
            if (response.isSuccessful) {
                withContext(Dispatchers.Main) {
                    _expenses.value = response.body()!!
                }
            }
        }
    }
}