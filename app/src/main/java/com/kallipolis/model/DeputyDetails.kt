package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class DeputyDetails(
    @SerializedName("dados")
    val details: DeputyDetailsData
)