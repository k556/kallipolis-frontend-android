package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class FavoriteDeputyItem(
    @SerializedName("id_deputado")
    val deputyId: Int
)