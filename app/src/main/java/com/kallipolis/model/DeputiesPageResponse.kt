package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class DeputiesPageResponse(
    @SerializedName("items")
    val items: List<DeputyResponseItem>,
    @SerializedName("maxPage")
    val maxPage: Int,
    @SerializedName("page")
    val page: Int,
    @SerializedName("totalItems")
    val totalItems: Int
)