package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class PoliticalPartyDetails(
    @SerializedName("deputadoLider")
    val deputyGenericInfo: DeputyGenericInfo,
    @SerializedName("id")
    val id: Int,
    @SerializedName("idLegislatura")
    val idLegislature: String,
    @SerializedName("lider")
    val leader: Int,
    @SerializedName("nome")
    val name: String,
    @SerializedName("numeroEleitoral")
    val electoralNumber: Any,
    @SerializedName("sigla")
    val partyInitials: String,
    @SerializedName("situacao")
    val status: String,
    @SerializedName("totalMembros")
    val totalMembers: Int,
    @SerializedName("totalPosse")
    val totalMandate: Int,
    @SerializedName("uri")
    val uri: String,
    @SerializedName("uriMembros")
    val uriMembers: String,
    @SerializedName("urlFacebook")
    val urlFacebook: Any,
    @SerializedName("urlLogo")
    val urlLogo: String,
    @SerializedName("urlWebsite")
    val urlWebsite: Any
)