# Kallipolis Android

Kallipolis é um sistema com a finalidade de acompanhar políticos, partidos e
votações na Câmara dos Deputados do Brasil. 

## Introdução

Este repositório possui o código do aplicativo que serve como frontend da aplicação. 
Informações sobre a plataforma backend estão concentradas no repositório
kallipolis-backend.

Os relatórios de progresso do projeto podem ser acessados [neste link](relatorio).

## Como executar o projeto

### Requerimentos
- Android SDK (versão 24+)

### Execução

Existem duas maneiras de executar o projeto:

1. Importar e executar o projeto no Android Studio.

2. Gerar uma build com utilizando a linha de comando

```shell
./gradlew clean app:assembleDebug

```
O comando acima criará um pacote apk que poderá ser executado utilizando um emulador ou um aparelho 
físico. O pacote estará na pasta abaixo.
```shell
app/build/outputs/apk/debug
```

Na [documentação oficial do Android](https://developer.android.com/studio/run) existem mais detalhes
sobre as duas formas de execução do aplicativo mencionadas acima.

### Bibliotecas utilizadas
- Kotlin
- Gradle
- Android SDK
