# Relatórios de progresso do projeto

## Introdução
Esta é a lista de relatórios de progresso do Projeto Final da disciplina *MAC5714 - Tópicos 
Avançados de Programação Orientada a Objetos* do programa de Pós Graduação em Ciência da Computação 
do IME/USP ministrada pelo Professor Dr. Fábio Kon.

Caso procure detalhes da implementação desenvolvida, é indicado olhar nos repositórios dedicados 
do [frontend](https://gitlab.com/k556/kallipolis-frontend-android) e do 
[backend](https://gitlab.com/k556/kallipolis-backend) da aplicação.

### Participantes do Grupo

| Nome                   | NUSP    |
|------------------------|---------|
| Alex Barboza           |         |
| Bruno Assis            |         |
| Caroline Araújo        |         |
| Gustavo Oliveira       |         |
| Hussani Oliveira       |         |
| Kouao Jean Vincent Mea | 9253169 |

## Lista de relatórios

Abaixo estão detalhados os requisitos de cada fase do projeto, a data de entrega definida e um link 
para cada documento com detalhes referentes a esta fase.

### Fase 1

Data de entrega: 15/10/2021 

Requisitos:
1. Por meio de uma página Web do seu sistema, um usuário deve ser capaz de fornecer (A) uma lista 
   de partidos políticos que ele gostaria de monitorar e (B) uma lista de deputados federais que 
   ele gostaria de monitorar.
2. O sistema deve permitir remover e listar partidos e deputados, sempre de forma intuitiva e 
   visualmente agradável.

[Link para detalhes](fase-1.md).

### Fase 2

Data de entrega: 05/11/2021

Requisitos:
1. O usuário deve então ser capaz de inserir um conjunto de palavras-chave ou expressões (i.e., 
   sequências de palavras separadas por espaço) indicando tópicos que ele gostaria de monitorar. 
   Por exemplo "Meio-ambiente", "Saúde da mulher", "Educação infantil" ou "Ciência".
2. Dado um período definido pelo usuário, o sistema deve carregar e mostrar de forma bonita e bem 
   organizada um resumo de todos os votos das entidades selecionadas (partidos ou deputados). Esse 
   resumo deve ser mostrado tanto em forma de texto, quanto em forma visual em gráficos.

[Link para detalhes](fase-2.md).

### Fase 3

Data de entrega: 26/11/2021

Requisitos:
1. O sistema deve montar e mostrar de forma bonita e bem organizada os votos de acordo com as 
   temáticas indicadas no item 3).

[Link para detalhes](fase-3.md).

### Fase 4

Data de entrega: 17/12/2021

Requisitos:
1. Ao definir as palavras-chave e expressões, o usuário deve poder usar expressões regulares
2. De uma forma bonita e elegante, o sistema deve mostrar duas nuvens de palavras distintas  
   representando os votos SIM e NÃO das entidades selecionadas (partidos ou deputados). O sistema 
   deve também mostrar uma nuvem de palavras representando os projeto de lei propostos por cada 
   entidade selecionada.
3. Opcional (ponto bônus): Sugira uma nova funcionalidade ainda não especificada acima. Ou então, 
   incorpore no sistema um relato dos gastos de um partido ou deputados de acordo com  
   https://www2.camara.leg.br/transparencia/receitas-e-despesas/despesas/pesquisa-de-despesas.

[Link para detalhes](fase-4.md). 
