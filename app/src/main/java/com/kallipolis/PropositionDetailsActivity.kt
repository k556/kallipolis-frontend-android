package com.kallipolis

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.google.android.material.shape.MaterialShapeDrawable
import com.kallipolis.databinding.ActivityPropositionDetailsBinding
import com.kallipolis.ui.propositiondetails.PropositionInfosDetailsFragment
import com.kallipolis.ui.propositiondetails.PropositionVotingInfoWebviewFragment
import com.kallipolis.utils.StorageUtils

class PropositionDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPropositionDetailsBinding
    private var propositionId: Int = 0
    private var propositionTitle: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_proposition_details)
        val propositionDetailsSlidePagerAdapter = PropositionDetailsSlidePagerAdapter(supportFragmentManager, this)
        binding.propositionDetailsViewPager.adapter = propositionDetailsSlidePagerAdapter
        binding.tabsPropositionDetails.setupWithViewPager(binding.propositionDetailsViewPager)
        initAcitivityArguments()
        initViews()
    }

    private fun initViews() {
        actionBar?.hide()
        supportActionBar?.hide()
        binding.propositionDetailsToolbarTitle.text = this.propositionTitle
        val window = this.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = this.resources.getColor(R.color.status_bar_color)
        val materialToolbarShape = binding.propositionDetailsToolbar.background as MaterialShapeDrawable
        materialToolbarShape.elevation = HomeActivity.HOME_NAV_ELEMENTS_ELEVATION
        materialToolbarShape.shadowCompatibilityMode =
            MaterialShapeDrawable.SHADOW_COMPAT_MODE_ALWAYS
        materialToolbarShape.shapeAppearanceModel = materialToolbarShape.shapeAppearanceModel
            .toBuilder()
            .setBottomLeftCornerSize(HomeActivity.TOOLBAR_BORDER_RADIUS)
            .setBottomRightCornerSize(HomeActivity.TOOLBAR_BORDER_RADIUS)
            .build()
    }

    private fun initAcitivityArguments() {
        this.propositionId = intent.getIntExtra(StorageUtils.PROPOSITION_ID_PARAMETER, 0)
        this.propositionTitle = intent.getStringExtra(StorageUtils.PROPOSITION_TITLE_PARAMETER)!!
    }

    private inner class PropositionDetailsSlidePagerAdapter(fm: FragmentManager,
                                                            private val context: Context):
        FragmentStatePagerAdapter(fm) {
        private val numPages = 2

        override fun getCount(): Int {
            return numPages
        }

        override fun getItem(position: Int): Fragment {
            val args = Bundle()
            args.putInt(StorageUtils.PROPOSITION_ID_PARAMETER, propositionId)
            return if (position == 0) {
                val propositionInfosDetailsFragment = PropositionInfosDetailsFragment()
                propositionInfosDetailsFragment.arguments = args
                propositionInfosDetailsFragment
            } else {
                val propositionVotingInfoWebviewFragment = PropositionVotingInfoWebviewFragment()
                propositionVotingInfoWebviewFragment.arguments = args
                propositionVotingInfoWebviewFragment
            }
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> this.context.getString(R.string.proposition_details_title)
                else -> this.context.getString(R.string.proposition_details_voting)
            }
        }
    }

}