package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.User
import com.kallipolis.model.UserResponse
import com.kallipolis.repository.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginActivityViewModel: ViewModel() {
    var currentUser = MutableLiveData<UserResponse?>()
    private val userRepository = UserRepository()

    private var _isValidUserInfo = MutableLiveData<Boolean>()
    val isValidUserInfo: LiveData<Boolean> = _isValidUserInfo

    init {
        _isValidUserInfo.value = false
    }

    fun setUser(userName: String, userEmail: String): Unit {
        if (isValidUsername(userName) && isValidUserEmail(userEmail)) {
            val user = User(userEmail, userName)
            CoroutineScope(Dispatchers.IO).launch {
                val userResponse = userRepository.createUser(user)
                withContext(Dispatchers.Main) {
                    currentUser.value = userResponse.body()
                }
            }
        }
    }

    private fun isValidUsername(userName: String): Boolean = !userName.isNullOrEmpty()

    private fun isValidUserEmail(userEmail: String): Boolean {
        if (userEmail.isNullOrEmpty())
            return false

        return android.util.Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()
    }

     fun validateUserLoginInfo(userName: String, userEmail: String) {
         _isValidUserInfo.value = isValidUsername(userName) && isValidUserEmail(userEmail)
    }

}