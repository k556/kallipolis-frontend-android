package com.kallipolis.ui.allpoliticalparties

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.kallipolis.R
import com.kallipolis.model.PoliticalPartyDetails
import de.hdodenhof.circleimageview.CircleImageView

class PartyDetailsPopup {


    private lateinit var popupPartyName: TextView
    private lateinit var popupPartyMembers: TextView
    private lateinit var popupPartyMandates: TextView
    private lateinit var popupPartyFullName: TextView
    private lateinit var popupPartyInitials: TextView
    private lateinit var partyLogoImg: CircleImageView
    private lateinit var popupPartyLeaderName: TextView
    private lateinit var popupPartyLeaderState: TextView
    private lateinit var popupPartyLeaderEmail: TextView
    private lateinit var popupPartyLeaderInfo: CardView

    companion object ConstantsHelper {
        const val POPUP_SHOW_BLUR: Int = 40
        const val POPUP_HIDE_BLUR: Int = 0
    }

    fun showPartyDetailsPopup(
        view: View,
        backgroundFrameLayout: FrameLayout,
        details: PoliticalPartyDetails,
        context: Context) {
        val inflaterService = view.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
        val inflater: LayoutInflater = inflaterService as LayoutInflater
        val popupView = inflater.inflate(R.layout.popup_party_details, null)
        val height = ConstraintLayout.LayoutParams.WRAP_CONTENT
        val width = ConstraintLayout.LayoutParams.WRAP_CONTENT
        val focusable = true
        val popuWindow = PopupWindow(popupView, width, height, focusable)

        backgroundFrameLayout.foreground.alpha = POPUP_SHOW_BLUR
        popuWindow.setOnDismissListener {
            backgroundFrameLayout.foreground.alpha = POPUP_HIDE_BLUR
        }

        initViews(popupView)
        initViewsContent(details, context)

        popuWindow.showAtLocation(view, Gravity.CENTER, 0, 0)
    }

    private fun initViews(view: View) {
        popupPartyName = view.findViewById(R.id.popup_party_name)
        partyLogoImg = view.findViewById(R.id.popup_party_profile_picture)
        popupPartyFullName = view.findViewById(R.id.popup_party_full_name)
        popupPartyInitials = view.findViewById(R.id.popup_party_initials)
        popupPartyMembers = view.findViewById(R.id.popup_party_members)
        popupPartyMandates = view.findViewById(R.id.popup_party_mandates)
        popupPartyLeaderName = view.findViewById(R.id.popup_party_leader_name)
        popupPartyLeaderState = view.findViewById(R.id.popup_party_leader_state)
        popupPartyLeaderEmail = view.findViewById(R.id.popup_party_leader_email)
        popupPartyLeaderInfo = view.findViewById(R.id.popup_party_leader_info)
    }

    private fun initViewsContent(details: PoliticalPartyDetails, context: Context) {
        if (isLogoUrlAvailable(details.urlLogo)) {
            Glide.with(context)
                .load(details.urlLogo)
                .into(partyLogoImg)
        } else {
            partyLogoImg.visibility = View.GONE
        }

        if (details.deputyGenericInfo != null) {
            val leaderState = "${details.deputyGenericInfo.partyInitials} " +
                    "· ${details.deputyGenericInfo.stateInitials}"
            popupPartyLeaderName.text = details.deputyGenericInfo.name
            popupPartyLeaderState.text = leaderState
            popupPartyLeaderEmail.text = details.deputyGenericInfo.email
        } else {
            popupPartyLeaderInfo.visibility = View.GONE
        }

        popupPartyName.text = details.name
        popupPartyFullName.text = details.name
        popupPartyInitials.text = details.partyInitials
        popupPartyMembers.text = details.totalMembers.toString()
        popupPartyMandates.text = details.totalMandate.toString()

    }

    private fun isLogoUrlAvailable(urlLogo: String): Boolean {
        return urlLogo != null && !urlLogo.isEmpty()
    }

}