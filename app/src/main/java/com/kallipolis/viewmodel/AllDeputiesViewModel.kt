package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.DeputyResponseItem
import com.kallipolis.repository.AllDeputiesRepository
import com.kallipolis.repository.FavoritesRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AllDeputiesViewModel : ViewModel(), DeputiesHandlerAdapter {

    private var currentPageIndex = 0

    private val _allDeputies = MutableLiveData<MutableList<DeputyResponseItem>>()
    val allDeputies: LiveData<MutableList<DeputyResponseItem>> = _allDeputies

    private val _shouldDisplayLoading = MutableLiveData<Boolean>()
    val shouldDisplayLoading: LiveData<Boolean> = _shouldDisplayLoading

    private val _newFavoriteDeputyPosition = MutableLiveData<Int>()
    val newFavoriteDeputyPosition: LiveData<Int> = _newFavoriteDeputyPosition

    private val _deletedFavoriteDeputyPosition = MutableLiveData<Int>()
    val deletedFavoriteDeputyPosition: LiveData<Int> = _deletedFavoriteDeputyPosition

    private val allDeputiesRepository = AllDeputiesRepository()
    private val favoritesRepository = FavoritesRepository()

    init {
        _shouldDisplayLoading.value = false
        _allDeputies.value = mutableListOf()
    }

    fun getAllDeputies(userId: Int) {
        toggleLoadingVisibility()

        CoroutineScope(Dispatchers.IO).launch {
            val fetchedDeputiesResponse = fetchDeputiesPage(userId)
            withContext(Dispatchers.Main) {
                notifyViews(fetchedDeputiesResponse)
            }
            increasePageIndex()
        }
    }

    private suspend fun fetchDeputiesPage(userId: Int): List<DeputyResponseItem> {
        val response = allDeputiesRepository.getAllDeputies(userId, currentPageIndex).body()
        return response!!.items
    }

    override fun addNewFavoriteDeputy(userId: Int, deputyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = favoritesRepository.addNewFavoriteDeputy(userId, deputyId)
            val newFavoriteDeputyId = response?.get(0)?.idDeputado
            setNewFavorite(newFavoriteDeputyId!!)
        }
    }

    override fun showDeputyDetails(deputyId: Int) {
        /*DO NOTHING*/
    }

    override fun deleteFavoriteDeputy(userId: Int, deputyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            favoritesRepository.deleteFavoriteDeputy(userId, deputyId)
            setDeletedFavorite(deputyId)
        }
    }

    private fun notifyViews(fetchedDeputiesResponse: List<DeputyResponseItem>) {
        val oldDeputies = _allDeputies.value!!
        val fetchedDeputies = fetchedDeputiesResponse.toMutableList()
        val newDeputiesList = oldDeputies + fetchedDeputies
        _allDeputies.value = newDeputiesList.toMutableList()
        toggleLoadingVisibility()
    }

    private suspend fun setNewFavorite(newFavoriteDeputyId: Int) {
        var i = 0
        for (deputy in _allDeputies.value!!) {
            if (deputy.id == newFavoriteDeputyId)  {
                withContext(Dispatchers.Main) {
                    _newFavoriteDeputyPosition.value = i
                }

                break
            }
            i++
        }
    }

    private suspend fun setDeletedFavorite(deletedDeputyId: Int) {
        var i = 0
        for (deputy in _allDeputies.value!!) {
            if (deputy.id == deletedDeputyId) {
                withContext(Dispatchers.Main) {
                    _deletedFavoriteDeputyPosition.value = i
                }
                break
            }
            i++
        }
    }

    private fun toggleLoadingVisibility() { _shouldDisplayLoading.value = !_shouldDisplayLoading.value!! }

    private fun increasePageIndex() = currentPageIndex++
}