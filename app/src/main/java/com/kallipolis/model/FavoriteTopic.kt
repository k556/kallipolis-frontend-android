package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class FavoriteTopic(
    @SerializedName("topicos")
    val newFavoriteLawPropositionTopics: List<Int>
)