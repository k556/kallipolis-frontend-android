package com.kallipolis.ui.propositiondetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kallipolis.databinding.FragmentPropositionVotingInfoWebviewBinding
import com.kallipolis.utils.StorageUtils

class PropositionVotingInfoWebviewFragment : Fragment() {

    private var _binding: FragmentPropositionVotingInfoWebviewBinding? = null
    private val binding get() = _binding!!
    private val baseWebviewUrl =
        "http://kallipolis-webviews.s3-website-us-east-1.amazonaws.com/votacao-prop/"

    private var propositionId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPropositionVotingInfoWebviewBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initFragmentArguments()
        loadWebviewVotingInfoData()
        return root
    }

    private fun initFragmentArguments() {
        this.propositionId = arguments?.getInt(StorageUtils.PROPOSITION_ID_PARAMETER)!!
    }

    private fun loadWebviewVotingInfoData() {
        val url = "${baseWebviewUrl}${this.propositionId}"
        binding.propositionInfoVotingDetailsWebview.settings.builtInZoomControls = false
        binding.propositionInfoVotingDetailsWebview.settings.javaScriptEnabled = true
        binding.propositionInfoVotingDetailsWebview.settings.domStorageEnabled = true
        binding.propositionInfoVotingDetailsWebview.loadUrl(url)
    }

}