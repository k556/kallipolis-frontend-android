package com.kallipolis

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.app.AppCompatDelegate
import androidx.databinding.DataBindingUtil
import com.kallipolis.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.delay

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    companion object {
        const val FETCHING_TIMEOUT = 3000L
        const val FADEIN_DURATION = 2500L
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        initViews()
        fetchSessionStorageDataAndNavigate()

    }

    private fun initViews() {
        actionBar?.hide()
        supportActionBar?.hide()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        val window = this.window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.statusBarColor = this.resources.getColor(R.color.kallipolis_primary)

        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator()
        fadeIn.duration = FADEIN_DURATION

        binding.splashScreenTitle.startAnimation(fadeIn)
        binding.splashScreenIcon.startAnimation(fadeIn)
    }

    private fun fetchSessionStorageDataAndNavigate() {
        CoroutineScope(Dispatchers.Main).launch {
            val isAlreadyLoggedIn = async(Dispatchers.IO) { hasCurrentActiveSession() }.await()
            if (!isAlreadyLoggedIn) {
                val intentToLogin = Intent(this@MainActivity, LoginActivity::class.java)
                startActivity(intentToLogin)
            }
        }
    }

    private suspend fun hasCurrentActiveSession(): Boolean {
        delay(FETCHING_TIMEOUT)
        return false
    }
}