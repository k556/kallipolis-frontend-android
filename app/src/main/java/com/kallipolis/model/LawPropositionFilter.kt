package com.kallipolis.model

import android.os.Parcel
import android.os.Parcelable
import java.lang.StringBuilder
import java.util.*

class LawPropositionFilter(parcel: Parcel? = null): Parcelable {
    private var dateStart: String = ""
    get() = field

    private var dateEnd: String = ""
    get() = field

    var brazilianFormatStartDate =""
    get() = field

    var brazilianFormatEndDate = ""
    get() = field

    private var listOfPropositionTopicCodes: MutableList<String> = mutableListOf()

    private var filterQueryString = StringBuilder()

    init {
//        p0?.writeString(dateStart)
//        p0?.writeString(dateEnd)
//        p0?.writeString(brazilianFormatStartDate)
//        p0?.writeString(brazilianFormatEndDate)
//        p0?.writeList(listOfPropositionTopicCodes)

        if (parcel != null) {
            this.dateStart = parcel.readString()!!
            this.dateEnd = parcel.readString()!!
            this.brazilianFormatStartDate = parcel.readString()!!
            this.brazilianFormatEndDate = parcel.readString()!!
            this.listOfPropositionTopicCodes = mutableListOf<String>().apply {
                parcel.readList(this, String::class.java.classLoader)
            }
        }
    }


    fun addNewPropositionTopicToFilter(topicId: Int) {
        listOfPropositionTopicCodes.add(topicId.toString())
    }

    fun removePropositionTopicFromFilter(topicId: Int) {
        val topicIdStr = topicId.toString()
        val newListOfPropositionTopicCodes = mutableListOf<String>()
        for (currentTopicId in listOfPropositionTopicCodes) {
            if (topicIdStr != currentTopicId) {
                newListOfPropositionTopicCodes.add(topicId.toString())
            }
        }

        this.listOfPropositionTopicCodes = newListOfPropositionTopicCodes
    }

    fun addDateRangeToFilter(startDate: Long, endDate: Long) {
        this.dateStart = formatDate(startDate)
        this.dateEnd = formatDate(endDate)
        this.brazilianFormatStartDate = formateDateToBrazilianFormat(startDate)
        this.brazilianFormatEndDate = formateDateToBrazilianFormat(endDate)
    }

    fun buildFilterQueryString(): String {
        if (!isValidFilter()) {
            return ""
        }

        if (hasValidTopicFilters()) {
            buildTopicsPropositionsFilter()
        }

        if (hasValidDateRangeFilter()) {
            buildDateRangePropositionsFilter()
        }

        return filterQueryString.toString()
    }

    private fun buildTopicsPropositionsFilter() {
        var i = 0
        filterQueryString.append("&codTema=")
        for (topic in listOfPropositionTopicCodes) {
            filterQueryString.append(topic)
            if (i != (listOfPropositionTopicCodes.size - 1)) {
                filterQueryString.append(",")
            }

            i++
        }
    }

    private fun buildDateRangePropositionsFilter() {
        filterQueryString.append("&dataStart=")
        filterQueryString.append(dateStart)
        filterQueryString.append("&dataEnd=")
        filterQueryString.append(dateEnd)
    }

    fun isValidFilter(): Boolean = (hasValidTopicFilters() || hasValidDateRangeFilter())
    private fun hasValidTopicFilters(): Boolean = listOfPropositionTopicCodes.size != 0
    private fun hasValidDateRangeFilter(): Boolean = !dateStart.isNullOrEmpty() && !dateEnd.isNullOrEmpty()

    private fun formatDate(timestampMiliseconds: Long): String {
        val date = Date()
        val gregorianDate = GregorianCalendar()
        date.time = timestampMiliseconds
        gregorianDate.time = date
        val strYear = gregorianDate.get(Calendar.YEAR).toString()

        val month = (gregorianDate.get(Calendar.MONTH)) + 1
        var strMonth = ""
        if (month <= SINGLE_DIGIT_DATE) {
            strMonth = "0$month"
        } else {
            strMonth = month.toString()
        }

        val day = gregorianDate.get(Calendar.DAY_OF_MONTH)
        var strDay = ""
        if (day <= SINGLE_DIGIT_DATE) {
            strDay = "0${day}"
        } else {
            strDay = day.toString()
        }

        return "$strYear-$strMonth-$strDay"
    }


    private fun formateDateToBrazilianFormat(timestampMiliseconds: Long): String {
        val date = Date()
        val gregorianDate = GregorianCalendar()
        date.time = timestampMiliseconds
        gregorianDate.time = date
        val strYear = gregorianDate.get(Calendar.YEAR).toString()

        val month = (gregorianDate.get(Calendar.MONTH)) + 1
        var strMonth = ""
        if (month <= SINGLE_DIGIT_DATE) {
            strMonth = "0$month"
        } else {
            strMonth = month.toString()
        }

        val day = gregorianDate.get(Calendar.DAY_OF_MONTH)
        var strDay = ""
        if (day <= SINGLE_DIGIT_DATE) {
            strDay = "0${day}"
        } else {
            strDay = day.toString()
        }

        return "$strDay/$strMonth/$strYear"
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        p0?.writeString(dateStart)
        p0?.writeString(dateEnd)
        p0?.writeString(brazilianFormatStartDate)
        p0?.writeString(brazilianFormatEndDate)
        p0?.writeList(listOfPropositionTopicCodes)
    }

    companion object CREATOR : Parcelable.Creator<LawPropositionFilter> {
        const val SINGLE_DIGIT_DATE = 9

        override fun createFromParcel(parcel: Parcel): LawPropositionFilter {
            return LawPropositionFilter(parcel)
        }

        override fun newArray(size: Int): Array<LawPropositionFilter?> {
            return arrayOfNulls(size)
        }
    }
}
