package com.kallipolis.ui.deputydetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kallipolis.databinding.FragmentDeputyPropositionsWebviewBinding
import com.kallipolis.utils.StorageUtils

class DeputyPropositionsWebviewFragment : Fragment() {

    private var _binding: FragmentDeputyPropositionsWebviewBinding? = null
    private val binding get() = _binding!!
    private var deputId: Int = 0

    private val baseWebviewUrl =
        "http://kallipolis-webviews.s3-website-us-east-1.amazonaws.com/proposicao-deputado/"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDeputyPropositionsWebviewBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initFragmentArguments()
        loadWebviewVotingInfoData()
        return root
    }

    private fun initFragmentArguments() {
        this.deputId = arguments?.getInt(StorageUtils.DEPUTY_ID_PARAMETER)!!
    }

    private fun loadWebviewVotingInfoData() {
        val url = "${baseWebviewUrl}${this.deputId}"
        binding.deputyDetailsPropositionsInfo.settings.builtInZoomControls = false
        binding.deputyDetailsPropositionsInfo.settings.javaScriptEnabled = true
        binding.deputyDetailsPropositionsInfo.settings.domStorageEnabled = true
        binding.deputyDetailsPropositionsInfo.loadUrl(url)
    }
}