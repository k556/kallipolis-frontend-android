package com.kallipolis.repository

import com.kallipolis.https.FavoritesService
import com.kallipolis.https.RetrofitInstance
import com.kallipolis.model.*

class FavoritesRepository {
    private var favoritesService: FavoritesService = RetrofitInstance
        .getRetrofitSingletonInstance()
        .create(FavoritesService::class.java)

    suspend fun addNewFavoriteDeputy(userId: Int, deputyId: Int): FavoriteDeputyResponse? {
        val newFavoriteDeputy = FavoriteDeputy()
        newFavoriteDeputy.add(FavoriteDeputyItem(deputyId))
        val response = favoritesService.addNewFavoriteDeputy(userId, newFavoriteDeputy)
        return if (response.isSuccessful)
            response.body()
        else
            null
    }

    suspend fun getAllFavoriteDeputies(userId: Int): AllFavoriteDeputiesResponse? {
        val response = favoritesService.getAllFavoriteDeputies(userId)
        return response.body()
    }

    suspend fun deleteFavoriteDeputy(userId: Int, deputyId: Int) {
        val deletedDeputy = FavoriteDeputyItem(deputyId)
        favoritesService.deleteFavoriteDeputy(userId, deletedDeputy)
    }

    suspend fun addNewFavoriteParty(userId: Int, partyId: Int): FavoritePartyResponse? {
        val newFavoriteParty = FavoriteParty()
        newFavoriteParty.add(FavoritePartyItem(partyId))
        val response = favoritesService.addNewFavoriteParty(userId, newFavoriteParty)
        return response.body()
    }

    suspend fun getAllFavoriteParties(userId: Int): FavoritePartyResponse?  {
        val response = favoritesService.getAllFavoriteParties(userId)
        return response.body()
    }

    suspend fun deleteFavoriteParty(userId: Int, partyId: Int) {
        val deletedParty = FavoritePartyItem(partyId)
        favoritesService.deleteFavoriteParty(userId, deletedParty)
    }
}