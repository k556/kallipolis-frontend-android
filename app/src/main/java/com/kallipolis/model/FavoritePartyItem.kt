package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class FavoritePartyItem(
    @SerializedName("id_partido")
    val idParty: Int
)