package com.kallipolis.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.ViewModelProvider
import com.kallipolis.databinding.FragmentFavoritesBinding
import com.kallipolis.viewmodel.FavoritesViewModel

class FavoritesFragment : Fragment() {

    private lateinit var favoritesViewModel: FavoritesViewModel
    private var _binding: FragmentFavoritesBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        favoritesViewModel =
            ViewModelProvider(this).get(FavoritesViewModel::class.java)

        _binding = FragmentFavoritesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val favoritesSlidePagerAdapter = FavoritesSlidePagerAdapter(parentFragmentManager)
        binding.favoritesViewPager.adapter = favoritesSlidePagerAdapter
        binding.tabsFavorites.setupWithViewPager(binding.favoritesViewPager)

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private inner class FavoritesSlidePagerAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm) {
        private val numPages = 2

        override fun getCount(): Int {
            return numPages
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> FavoritesDeputiesFragment()
                else -> FavoritesPartiesFragment()
            }
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return when (position) {
                0 -> "Deputados"
                else -> "Partidos"
            }
        }
    }
}