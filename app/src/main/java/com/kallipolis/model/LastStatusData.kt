package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class LastStatusData(
    @SerializedName("condicaoEleitoral")
    val electoralStatus: String,
    @SerializedName("descricaoStatus")
    val statusDescription: Any,
    @SerializedName("email")
    val email: String,
    @SerializedName("gabinete")
    val cabinetInfo: CabinetInfo,
    @SerializedName("id")
    val id: Int,
    @SerializedName("idLegislatura")
    val idLegislature: Int,
    @SerializedName("nome")
    val name: String,
    @SerializedName("nomeEleitoral")
    val electoralName: String,
    @SerializedName("siglaPartido")
    val partyInitials: String,
    @SerializedName("siglaUf")
    val stateInitials: String,
    @SerializedName("situacao")
    val termStatus: String,
    @SerializedName("uri")
    val uri: String,
    @SerializedName("uriPartido")
    val partyUri: String,
    @SerializedName("urlFoto")
    val profilePictureUrl: String
)