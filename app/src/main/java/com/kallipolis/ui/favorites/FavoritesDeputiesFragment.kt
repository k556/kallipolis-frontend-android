package com.kallipolis.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.kallipolis.databinding.FragmentDeputiesFavoritesBinding
import com.kallipolis.ui.alldeputies.DeputyItemCardViewAdapter
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.FavoriteDeputiesViewModel

class FavoritesDeputiesFragment: Fragment() {

    private var deputyItemCardViewAdapter: DeputyItemCardViewAdapter? = null
    private lateinit var favoriteDeputiesViewModel: FavoriteDeputiesViewModel
    private var _binding: FragmentDeputiesFavoritesBinding? = null
    private val binding get() = _binding!!
    private var currentUserId = StorageUtils.INVALID_USER_ID

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        favoriteDeputiesViewModel = ViewModelProvider(this).get(FavoriteDeputiesViewModel::class.java)
        _binding = FragmentDeputiesFavoritesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        initObservers()
        initViewData()
        initViews()
        return root
    }

    private fun initObservers() {
        favoriteDeputiesViewModel.allFavoriteDeputies.observe(viewLifecycleOwner, {
            if (it.size == 0) {
                binding.containerNoFavoriteDeputy.visibility = View.VISIBLE
                binding.allFavoriteDeputiesRecyclerView.visibility = View.GONE
            } else {
                binding.containerNoFavoriteDeputy.visibility = View.GONE
                binding.allFavoriteDeputiesRecyclerView.visibility = View.VISIBLE

                val deputies = ArrayList(it)
                if (deputyItemCardViewAdapter == null) {
                    deputyItemCardViewAdapter =
                        DeputyItemCardViewAdapter(deputies, favoriteDeputiesViewModel, currentUserId)
                    binding.allFavoriteDeputiesRecyclerView.adapter = deputyItemCardViewAdapter
                } else {
                    deputyItemCardViewAdapter!!.updateData(deputies)
                }
            }
        })
    }

    private fun retrieveUserId():Int {
        return StorageUtils.retrieveUserId(requireContext())
    }

    private fun initViewData() {
        currentUserId = retrieveUserId()
        favoriteDeputiesViewModel.getAllFavoriteDeputies(currentUserId)
    }

    private fun initViews() {
        binding.favoriteDeputiesFrameLayout.foreground.alpha = 0
        binding.allFavoriteDeputiesRecyclerView.layoutManager = LinearLayoutManager(context)
    }

}