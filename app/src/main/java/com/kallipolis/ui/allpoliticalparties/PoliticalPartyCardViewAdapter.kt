package com.kallipolis.ui.allpoliticalparties

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.kallipolis.R
import com.kallipolis.databinding.PoliticalPartyListItemBinding
import com.kallipolis.model.PoliticalPartyResponseItem
import com.kallipolis.viewmodel.PartiesHandlerAdapter

class PoliticalPartyCardViewAdapter(private var allPoliticalParties: ArrayList<PoliticalPartyResponseItem>,
                                    private val partiesHandlerAdapter: PartiesHandlerAdapter,
                                    private val userId: Int)
    : RecyclerView.Adapter<PoliticalPartyItemViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PoliticalPartyItemViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: PoliticalPartyListItemBinding = DataBindingUtil
            .inflate(layoutInflater, R.layout.political_party_list_item, parent, false)

        return PoliticalPartyItemViewHolder(binding, parent.context, partiesHandlerAdapter, userId)
    }

    override fun onBindViewHolder(holder: PoliticalPartyItemViewHolder, position: Int) {
        holder.bind(allPoliticalParties[position])
    }

    override fun getItemCount(): Int {
        return allPoliticalParties.size
    }

    fun setNewFavorite(position: Int) {
        val newFavoriteParty = this.allPoliticalParties[position]
        newFavoriteParty.isFavorite = true
        this.notifyDataSetChanged()
    }

    fun unsetFavorite(position: Int) {
        val party = this.allPoliticalParties[position]
        party.isFavorite = false
        this.notifyDataSetChanged()
    }

    fun updateData(newItems: ArrayList<PoliticalPartyResponseItem>) {
        this.allPoliticalParties = newItems
        this.notifyDataSetChanged()
    }
}

class PoliticalPartyItemViewHolder(private val binding: PoliticalPartyListItemBinding,
                                   private val context: Context,
                                   private val partiesHandlerAdapter: PartiesHandlerAdapter,
                                   private val userId: Int):
    RecyclerView.ViewHolder(binding.root) {
    fun bind(politicalParty: PoliticalPartyResponseItem) {
        val currentPartyId = politicalParty.id
        binding.politicalPartyInitials.text = politicalParty.partyInitials
        binding.politicalPartyName.text = politicalParty.name
        binding.politicalPartyLeader.text = ""

        Glide.with(context)
            .load(politicalParty.urlLogo)
            .into(binding.politicalPartyThumbnail)

        if (politicalParty.isFavorite) {
            binding.icFavoriteParty.setImageResource(R.drawable.ic_icon_filled)
        } else {
            binding.icFavoriteParty.setImageResource(R.drawable.ic_icon_empty)
        }

        binding.icFavoriteParty.setOnClickListener {
            if (politicalParty.isFavorite) {
                partiesHandlerAdapter.deleteFavoriteParty(userId, currentPartyId)
            } else {
                partiesHandlerAdapter.addNewFavoriteParty(userId, currentPartyId)
            }
        }

        binding.politicalPartyDetailsContainer.setOnClickListener {
            partiesHandlerAdapter.showPartyDetails(currentPartyId)
        }

    }
}