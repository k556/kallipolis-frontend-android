package com.kallipolis.repository

import com.kallipolis.https.PoliticalPartyService
import com.kallipolis.https.RetrofitInstance

class PoliticalPartiesRepository {
    private var politicalPartiesWebService: PoliticalPartyService = RetrofitInstance
        .getRetrofitSingletonInstance()
        .create(PoliticalPartyService::class.java)

    suspend fun getAllPoliticalParties(userId: Int) = politicalPartiesWebService.fetchAllPoliticalParties(userId)
    suspend fun getPoliticalPartyDetails(partyId: Int) = politicalPartiesWebService.fetchPoliticalPartyDetails(partyId)
}