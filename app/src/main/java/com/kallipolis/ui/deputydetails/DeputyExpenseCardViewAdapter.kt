package com.kallipolis.ui.deputydetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kallipolis.R
import com.kallipolis.databinding.DeputyExpenseItemListBinding
import com.kallipolis.model.DeputyExpenseDetailsResponse
import com.kallipolis.utils.StringUtils.Companion.MONTH_APRIL
import com.kallipolis.utils.StringUtils.Companion.MONTH_AUGUST
import com.kallipolis.utils.StringUtils.Companion.MONTH_FEBRUARY
import com.kallipolis.utils.StringUtils.Companion.MONTH_JANUARY
import com.kallipolis.utils.StringUtils.Companion.MONTH_JULY
import com.kallipolis.utils.StringUtils.Companion.MONTH_JUNE
import com.kallipolis.utils.StringUtils.Companion.MONTH_MARCH
import com.kallipolis.utils.StringUtils.Companion.MONTH_MAY
import com.kallipolis.utils.StringUtils.Companion.MONTH_NOVEMBER
import com.kallipolis.utils.StringUtils.Companion.MONTH_OCTOBER
import com.kallipolis.utils.StringUtils.Companion.MONTH_SEPTEMBER
import java.math.BigDecimal
import java.math.RoundingMode

class DeputyExpenseCardViewAdapter(
    private var allExpenses: ArrayList<DeputyExpenseDetailsResponse>
): RecyclerView.Adapter<ExpenseItemCardViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpenseItemCardViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(parent.context)
        val binding: DeputyExpenseItemListBinding = DataBindingUtil
            .inflate(layoutInflater, R.layout.deputy_expense_item_list, parent, false)
        return ExpenseItemCardViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ExpenseItemCardViewHolder, position: Int) {
        holder.bind(allExpenses[position])
    }

    override fun getItemCount(): Int {
       return allExpenses.size
    }
}

class ExpenseItemCardViewHolder(val binding: DeputyExpenseItemListBinding):
    RecyclerView.ViewHolder(binding.root) {
        fun bind(expense: DeputyExpenseDetailsResponse) {
            val value = BigDecimal(expense.netValue).setScale(2, RoundingMode.HALF_EVEN)
            binding.deputyExpenseType.text = expense.expenseType
            binding.deputyExpenseProviderName.text = expense.providerName
            binding.deputyExpenseNetValue.text = value.toString()
            binding.deputyExpenseDate.text = getDate(expense)
        }

    fun getDate(expense: DeputyExpenseDetailsResponse): String {
        val month = when (expense.month) {
            MONTH_JANUARY   -> "Janeiro"
            MONTH_FEBRUARY  -> "Fevereiro"
            MONTH_MARCH     -> "Março"
            MONTH_APRIL     -> "Abril"
            MONTH_MAY       -> "Maio"
            MONTH_JUNE      -> "Junho"
            MONTH_JULY      ->  "Julho"
            MONTH_AUGUST    -> "Agosto"
            MONTH_SEPTEMBER -> "Setembro"
            MONTH_OCTOBER   -> "Outubro"
            MONTH_NOVEMBER  -> "Novembro"
            else -> "Dezembro"
        }

        return "$month de ${expense.year}"
    }
}