package com.kallipolis.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.kallipolis.databinding.FragmentPartiesFavoritesBinding
import com.kallipolis.ui.allpoliticalparties.PartyDetailsPopup
import com.kallipolis.ui.allpoliticalparties.PoliticalPartyCardViewAdapter
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.FavoritePartiesViewModel

class FavoritesPartiesFragment: Fragment() {

    private var politicalPartyCardViewAdapter: PoliticalPartyCardViewAdapter? = null
    private lateinit var favoritesPartiesViewModel: FavoritePartiesViewModel
    private var _bindinding: FragmentPartiesFavoritesBinding? = null
    private val binding get() = _bindinding!!
    private var currentUserId = StorageUtils.INVALID_USER_ID

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        favoritesPartiesViewModel = ViewModelProvider(this).get(FavoritePartiesViewModel::class.java)
        _bindinding = FragmentPartiesFavoritesBinding.inflate(inflater, container, false)
        val root: View = binding.root

        initObservers()
        initViewData()
        initViews()
        return root
    }

    private fun initObservers() {
        favoritesPartiesViewModel.allFavoriteParties.observe(viewLifecycleOwner, {
            if (it.isEmpty()) {
                binding.containerNoFavoriteParty.visibility = View.VISIBLE
                binding.allFavoritePartiesRecyclerView.visibility = View.GONE
            } else {
                binding.containerNoFavoriteParty.visibility = View.GONE
                binding.allFavoritePartiesRecyclerView.visibility = View.VISIBLE

                val parties = ArrayList(it)
                if (politicalPartyCardViewAdapter == null) {
                    politicalPartyCardViewAdapter =
                        PoliticalPartyCardViewAdapter(parties, favoritesPartiesViewModel, currentUserId)
                    binding.allFavoritePartiesRecyclerView.adapter = politicalPartyCardViewAdapter
                } else {
                    politicalPartyCardViewAdapter!!.updateData(parties)
                }
            }
        })

        favoritesPartiesViewModel.selectedPartyDetails.observe(viewLifecycleOwner, {
            val popup = PartyDetailsPopup()
            popup.showPartyDetailsPopup(requireView(),
                binding.favoritePartiesFrameLayout,
                it, requireContext())
        })

    }

    private fun initViewData() {
        currentUserId = retrieveUserId()
        favoritesPartiesViewModel.getAllFavoriteParties(currentUserId)
    }

    private fun retrieveUserId():Int {
        return StorageUtils.retrieveUserId(requireContext())
    }

    private fun initViews() {
        binding.favoritePartiesFrameLayout.foreground.alpha = 0
        binding.allFavoritePartiesRecyclerView.layoutManager = LinearLayoutManager(context)
    }

}