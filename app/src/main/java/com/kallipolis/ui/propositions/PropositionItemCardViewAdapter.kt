package com.kallipolis.ui.propositions

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kallipolis.PropositionDetailsActivity
import com.kallipolis.R
import com.kallipolis.databinding.PropositionListItemBinding
import com.kallipolis.model.LawPropositionsResponseItem
import com.kallipolis.utils.StorageUtils

class PropositionItemCardViewAdapter(private var propositions: ArrayList<LawPropositionsResponseItem>,
                                    private val context: Context)
    : RecyclerView.Adapter<PropositionItemCardViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PropositionItemCardViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: PropositionListItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.proposition_list_item, parent, false)
        return PropositionItemCardViewHolder(binding, context)
    }

    override fun onBindViewHolder(holder: PropositionItemCardViewHolder, position: Int) {
        holder.bind(propositions[position])
    }

    override fun getItemCount(): Int {
        return propositions.size
    }

    fun updateData(newItems: ArrayList<LawPropositionsResponseItem>) {
        this.propositions = newItems
        this.notifyDataSetChanged()
    }
}

class PropositionItemCardViewHolder(val binding: PropositionListItemBinding, private val context: Context):
        RecyclerView.ViewHolder(binding.root) {

        fun bind(proposition: LawPropositionsResponseItem) {
            val title = "${proposition.idPropositionType}-${proposition.number}/${proposition.year}"
            binding.propositionTitle.text = title
            binding.propositionDescription.text = proposition.description
            binding.propositionCallToActionDetails.setOnClickListener {
                val intent = Intent(context, PropositionDetailsActivity::class.java)
                intent.putExtra(StorageUtils.PROPOSITION_ID_PARAMETER, proposition.id)
                intent.putExtra(StorageUtils.PROPOSITION_TITLE_PARAMETER, title)
                context.startActivity(intent)
            }
        }
}