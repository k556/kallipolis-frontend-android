package com.kallipolis.utils

import java.text.SimpleDateFormat
import java.util.*

class StringUtils private constructor() {
    companion object {
        const val DATE_SINGLE_DIGIT: Int = 9
        const val MONTH_JANUARY     = 1
        const val MONTH_FEBRUARY    = 2
        const val MONTH_MARCH       = 3
        const val MONTH_APRIL       = 4
        const val MONTH_MAY         = 5
        const val MONTH_JUNE        = 6
        const val MONTH_JULY        = 7
        const val MONTH_AUGUST      = 8
        const val MONTH_SEPTEMBER   = 9
        const val MONTH_OCTOBER     = 10
        const val MONTH_NOVEMBER    = 11

        fun parseDateToBrazilianFormat(strDate: String): String {
            val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val date: Date = formatter.parse(strDate)

            val birthDateDay: String = if (date.day <= DATE_SINGLE_DIGIT) "0${date.day}" else "${date.day}"
            var birthDateMonth: String = "${date.month + 1}"
            birthDateMonth = if (date.day <= DATE_SINGLE_DIGIT) "0${birthDateMonth}" else birthDateMonth

            return "${birthDateDay}/${birthDateMonth}/${date.year}"
        }

        fun capitalizeFirstLetterOfEachWord(str: String): String {
            return str.split(" ").joinToString(" ") { it.capitalize() }.trimEnd()
        }
    }
}