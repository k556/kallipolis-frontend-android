package com.kallipolis.repository

import com.kallipolis.https.LawPropositionsService
import com.kallipolis.https.RetrofitInstance
import com.kallipolis.model.FavoriteTopic
import com.kallipolis.model.LawPropositionFilter
import com.kallipolis.model.LawPropositionsResponse
import retrofit2.Response
import java.lang.StringBuilder

class LawPropositionsRepository {

    companion object {
        const val PAGE_SIZE: Int = 15
    }

    private var lawPropositionsService: LawPropositionsService = RetrofitInstance
        .getRetrofitSingletonInstance()
        .create(LawPropositionsService::class.java)

    suspend fun getAllLawPropositions(pageIndex: Int) =
        lawPropositionsService.fetchAllLawPropositions(pageIndex, PAGE_SIZE)

    suspend fun getAllLawPropositionThemes(userId: Int) =
        lawPropositionsService.fetchAllLawPropositionTopics(userId)

    suspend fun getLawPropositionsWithFilters(
        filter: LawPropositionFilter,
        currentPage: Int): Response<LawPropositionsResponse> {
        val url = StringBuilder()
        url.append(RetrofitInstance.BASE_URL)
        url.append("/proposicao?page=")
        url.append(currentPage)
        url.append("&size=")
        url.append(PAGE_SIZE)
        url.append(filter.buildFilterQueryString())
        return lawPropositionsService.fetchLawPropositionsWithFilters(url.toString())
    }

    suspend fun getLawPropositionDetails(propositionId: Int) =
        lawPropositionsService.fetchLawPropositionDetails(propositionId)

    suspend fun addNewFavoritePropositionTopics(userId: Int, favoriteTopic: FavoriteTopic) =
        lawPropositionsService.addNewFavoritePropositionTopics(userId, favoriteTopic)

    suspend fun deleteFavoritePropositionTopic(userId: Int, favoriteTopicId: Int) =
        lawPropositionsService.deleteFavoritePropositionTopic(userId, favoriteTopicId)

    suspend fun deleteAllFavoritePropositionTopics(userId: Int, favoriteTopicsIds: List<Int>):
            Response<Unit> {
        val url = StringBuilder()
        val queryUrlWithTopicIds = buildQueryUrlForFavoriteTopicsDeletion(favoriteTopicsIds)
        url.append(RetrofitInstance.BASE_URL)
        url.append("/usuario/${userId}")
        url.append("/votacao/topicos/favorito?topicos=")
        url.append(queryUrlWithTopicIds)
        return lawPropositionsService.deleteAllFavoritePropositionTopics(url.toString())
    }

    private fun buildQueryUrlForFavoriteTopicsDeletion(favoriteTopicsIds: List<Int>): String {
        val queryUrlWithTopicIds = StringBuilder()
        for ((i, topicId) in favoriteTopicsIds.withIndex()) {
            if (isLastElementInList(i, favoriteTopicsIds.size)) {
                queryUrlWithTopicIds.append(topicId)
            } else {
                queryUrlWithTopicIds.append("$topicId,")
            }
        }

        return queryUrlWithTopicIds.toString()
    }

    private fun isLastElementInList(position: Int, nElements: Int) = position == (nElements - 1)
}