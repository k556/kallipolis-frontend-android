package com.kallipolis.ui.deputydetails

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.kallipolis.databinding.DeputyExpensesFragmentBinding
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.DeputyExpensesViewModel

class DeputyExpensesFragment : Fragment() {
    private lateinit var viewModel: DeputyExpensesViewModel
    private var _binding: DeputyExpensesFragmentBinding? = null
    private val binding get() = _binding!!
    private var deputId: Int = 0
    private var deputyExpenseCardViewAdapter: DeputyExpenseCardViewAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(DeputyExpensesViewModel::class.java)
        _binding = DeputyExpensesFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initFragmentArguments()
        initObservers()
        initViewData()
        initViews()
        return root
    }

    private fun initFragmentArguments() {
        this.deputId = arguments?.getInt(StorageUtils.DEPUTY_ID_PARAMETER)!!
    }

    private fun initObservers() {
        viewModel.expenses.observe(viewLifecycleOwner, {
            val expensesList = ArrayList(it.deputyExpenseDetailsResponse)
            binding.deputyExpensesTotalAmount.text = "Total Gasto: R$ ${it.totalSpentInPeriod}"
            if (deputyExpenseCardViewAdapter == null) {
                deputyExpenseCardViewAdapter = DeputyExpenseCardViewAdapter(expensesList)
                binding.deputyExpensesRecyclerView.adapter = deputyExpenseCardViewAdapter
                binding.progressDeputyExpenses.visibility = View.GONE
                binding.deputyExpensesContainer.visibility = View.VISIBLE
            }
        })
    }

    private fun initViewData() {
        viewModel.getAllDeputyExpenses(deputId)
    }

    private fun initViews() {
        binding.deputyExpensesRecyclerView.layoutManager = LinearLayoutManager(context)
    }
}