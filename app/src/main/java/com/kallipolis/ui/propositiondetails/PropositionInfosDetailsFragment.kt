package com.kallipolis.ui.propositiondetails

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kallipolis.databinding.PropositionInfosDetailsFragmentBinding
import com.kallipolis.model.LawPropositionDetailsResponse
import com.kallipolis.utils.StorageUtils
import com.kallipolis.viewmodel.PropositionIsnfoDetailsViewModel

class PropositionInfosDetailsFragment : Fragment() {

    private lateinit var viewModel: PropositionIsnfoDetailsViewModel
    private var _binding: PropositionInfosDetailsFragmentBinding? = null
    private val binding get() = _binding!!
    private var propositionId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(PropositionIsnfoDetailsViewModel::class.java)
        _binding = PropositionInfosDetailsFragmentBinding.inflate(inflater, container, false)
        val root: View = binding.root
        initFragmentArguments()
        initObservers()
        fetchViewData()
        return root
    }

    private fun initFragmentArguments() {
        this.propositionId = arguments?.getInt(StorageUtils.PROPOSITION_ID_PARAMETER)!!
    }

    private fun initObservers() {
        viewModel.propositionDetails.observe(viewLifecycleOwner, {
            if (it != null) {
                fillViewData(it)
            }
        })
    }

    private fun fetchViewData() {
        viewModel.getPropositionDetails(this.propositionId)
    }

    private fun fillViewData(proposition: LawPropositionDetailsResponse) {
        /* fill views with incoming data from server */
        binding.propositionInfoDetailScopeText.text = proposition.scope
        binding.propositionInfoDetailAuthorName.text = proposition.lawPropositionAuthors[0].name
        binding.propositionInfoDetailTypeInitials.text = proposition.typeInitials
        binding.propositionInfoDetailTypeDescription.text = proposition.typeDescription
        binding.propositionInfoDetailNumber.text = proposition.number.toString()
        binding.propositionInfoDetailYear.text = proposition.year.toString()
        binding.propositionInfoDetailStatusScope.text = proposition.lawPropositionStatus.scope
        binding.propositionInfoDetailStatusRegime.text = proposition.lawPropositionStatus.regime
        val description = proposition.lawPropositionStatus.statusDescription
        if (description != null) {
            binding.propositionInfoDetailStatusStatusDescription.text =
                proposition.lawPropositionStatus.statusDescription
        } else {
            binding.propositionInfoDetailStatusDivider.visibility = View.GONE
            binding.propositionInfoDetailStatusDescriptionContainer.visibility = View.GONE
        }

        binding.propositionInfoDetailStatusDispatch.text = proposition.lawPropositionStatus.dispatch
        /* set the visibility for spinner and main container */
        binding.propositionInfoDetailSpinnerContainer.visibility = View.GONE
        binding.propositionInfoDetailMainContainerScroll.visibility = View.VISIBLE

    }
}