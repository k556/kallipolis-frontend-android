package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class LawPropositionAuthor(
    @SerializedName("codTipo")
    val idType: Int,
    @SerializedName("deputado")
    val deputy: DeputyGenericInfo,
    @SerializedName("nome")
    val name: String,
    @SerializedName("ordemAssinatura")
    val signatureOrder: Int,
    @SerializedName("proponente")
    val proponent: Int,
    @SerializedName("tipo")
    val type: String,
    @SerializedName("uri")
    val uri: String
)