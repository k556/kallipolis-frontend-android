package com.kallipolis.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kallipolis.model.PoliticalPartyDetails
import com.kallipolis.model.PoliticalPartyResponseItem
import com.kallipolis.repository.FavoritesRepository
import com.kallipolis.repository.PoliticalPartiesRepository
import kotlinx.coroutines.*

class AllPoliticalPartiesViewModel : ViewModel(), PartiesHandlerAdapter {
    private val _allPoliticalPartiesViewModel = MutableLiveData<List<PoliticalPartyResponseItem>>()
    val allPoliticalPartiesVM: LiveData<List<PoliticalPartyResponseItem>> = _allPoliticalPartiesViewModel

    private val _newFavoritePoliticalPartyPosition = MutableLiveData<Int>()
    val newFavoritePoliticalPartyPosition: LiveData<Int> = _newFavoritePoliticalPartyPosition

    private val _deletedFavoritePartyPosition = MutableLiveData<Int>()
    val deletedFavoritePartyPosition: LiveData<Int> = _deletedFavoritePartyPosition

    private val _selectedPartyDetails = MutableLiveData<PoliticalPartyDetails>()
    val selectedPartyDetails: LiveData<PoliticalPartyDetails> = _selectedPartyDetails

    private val allPoliticalPartiesRepository = PoliticalPartiesRepository()
    private val favoritesRepository = FavoritesRepository()

    fun getAllPoliticalParties(userId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val allParties: List<PoliticalPartyResponseItem> = fetchAllParties(userId)
            withContext(Dispatchers.Main) {
                _allPoliticalPartiesViewModel.value = allParties
            }
        }
    }

    override fun addNewFavoriteParty(userId: Int, partyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = favoritesRepository.addNewFavoriteParty(userId, partyId)
            val newFavoritePoliticalPartyId = response?.get(0)?.partyId
            setNewFavorite(newFavoritePoliticalPartyId!!)
        }
    }

    override fun deleteFavoriteParty(userId: Int, partyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            favoritesRepository.deleteFavoriteParty(userId, partyId)
            setDeletedFavorite(partyId)
        }
    }

    override fun showPartyDetails(partyId: Int) {
        CoroutineScope(Dispatchers.IO).launch {
            val response = allPoliticalPartiesRepository.getPoliticalPartyDetails(partyId)
            withContext(Dispatchers.Main) {
                _selectedPartyDetails.value = response.body()
            }
        }
    }

    private suspend fun fetchAllParties(userId: Int): List<PoliticalPartyResponseItem> {
        return allPoliticalPartiesRepository.getAllPoliticalParties(userId).body()!!
    }

    private suspend fun setNewFavorite(newFavoritePoliticalPartyId: Int) {
        var i = 0
        for (party in _allPoliticalPartiesViewModel.value!!) {
            if (party.id == newFavoritePoliticalPartyId) {
                withContext(Dispatchers.Main) {
                    _newFavoritePoliticalPartyPosition.value = i
                }
                break
            }
            i++
        }
    }

    private suspend fun setDeletedFavorite(deletedPartyId: Int) {
        var i = 0
        for (party in _allPoliticalPartiesViewModel.value!!) {
            if (deletedPartyId == party.id) {
                withContext(Dispatchers.Main) {
                    _deletedFavoritePartyPosition.value = i
                }
                break
            }
            i++
        }
    }

}