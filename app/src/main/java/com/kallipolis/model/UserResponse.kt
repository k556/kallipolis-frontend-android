package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("email")
    val email: String,
    @SerializedName("nome")
    val nome: String,
    @SerializedName("token")
    val token: String,
    @SerializedName("id")
    val id: Int
)