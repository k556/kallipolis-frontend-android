package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class LawPropositionStatus(
    @SerializedName("ambito")
    val scope: String,
    @SerializedName("dataHora")
    val dateTimestamp: String,
    @SerializedName("descricaoSituacao")
    val statusDescription: String,
    @SerializedName("descricaoTramitacao")
    val processingDescription: String,
    @SerializedName("despacho")
    val dispatch: String,
    @SerializedName("regime")
    val regime: String,
    @SerializedName("siglaOrgao")
    val organInitials: String,
    @SerializedName("uriOrgao")
    val organUri: String
)