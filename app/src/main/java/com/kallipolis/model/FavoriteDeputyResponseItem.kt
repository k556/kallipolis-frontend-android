package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class FavoriteDeputyResponseItem(
    @SerializedName("deputado")
    val deputado: Any,
    @SerializedName("id")
    val id: Int,
    @SerializedName("idDeputado")
    val idDeputado: Int,
    @SerializedName("idUsuario")
    val idUsuario: Int
)