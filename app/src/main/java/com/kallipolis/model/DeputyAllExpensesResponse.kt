package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class DeputyAllExpensesResponse(
    @SerializedName("dados")
    val deputyExpenseDetailsResponse: List<DeputyExpenseDetailsResponse>,
    @SerializedName("totalGastoNoPeriodo")
    val totalSpentInPeriod: Double
)