# Monitor Político - Kallipolis
<!-- ![alt_text](images/image1.png "image_tooltip") -->

- [Monitor Político - Kallipolis](#monitor-político---kallipolis)
  - [Introdução](#introdução)
    - [Participantes do Grupo](#participantes-do-grupo)
    - [Requisitos definidos para a entrega](#requisitos-definidos-para-a-entrega)
  - [Descrição](#descrição)
  - [Funcionalidades](#funcionalidades)
    - [Temas](#Temas)
    - [Proposições](#Proposições)
    - [Informações deputados/partidos](#Informações-deputados-e-partidos)
    - [Visualizar votos dos deputados](#visualizar-votos-dos-deputados-e-partidos)


## Introdução
Este o relatório da disciplina MAC5714 - Tópicos Avançados de Programação Orientada a Objetos do
programa de Pós Graduação em Ciência da Computação do IME/USP ministrada pelo Professor Dr. Fábio
Kon.

O objetivo desse projeto é auxiliar usuários a localizar e organizar informações sobre a atividade
parlamentar dos deputados e partidos. Para isto desenvolvemos e disponibilizamos um
[aplicativo Android na PlayStore](https://play.google.com/store/apps/details?id=com.kallipolis) que
possibilita a usuários escolher deputados e partidos ele deseja monitorar.

Caso procure detalhes da implementação desenvolvida, é indicado olhar nos repositórios dedicados
do [frontend](https://gitlab.com/k556/kallipolis-frontend-android) e do
[backend](https://gitlab.com/k556/kallipolis-backend) da aplicação.

### Participantes do Grupo

| Nome                   | NUSP    |
|------------------------|---------|
| Alex Barboza           |         |
| Bruno Assis            |         |
| Caroline Araújo        |         |
| Gustavo Oliveira       |         |
| Hussani Oliveira       |         |
| Kouao Jean Vincent Mea | 9253169 |


## Descrição

Data de entrega: 05/11/2021

### Requisitos definidos para a entrega
3. O usuário deve então ser capaz de inserir um conjunto de palavras-chave ou expressões (i.e., sequências de palavras separadas por espaço) indicando tópicos que ele gostaria de monitorar. Por exemplo "Meio-ambiente", "Saúde da mulher", "Educação infantil" ou "Ciência".
4. Dado um período definido pelo usuário, o sistema deve carregar e mostrar de forma bonita e bem organizada um resumo de todos os votos das entidades selecionadas (partidos ou deputados). Esse resumo deve ser mostrado tanto em forma de texto, quanto em forma visual em gráficos.

Conforme especificado nos requisitos acima, foram definidas as seguintes funcionalidades que um usuário poderá executar no sistema.

- [Temas](#Temas)
- [Proposições](#Proposições)
- [Informações deputados/partidos](#Informações-deputados-e-partidos)
- [Visualizar votos dos deputados](#visualizar-votos-dos-deputados-e-partidos)

## Funcionalidades

### Temas

Conforme a figura a seguir, o usuário poderá escolher o(s) tema(s) que ele gostaria de monitorar e se preferir especificar o período.

![Tela de Escolha dos Temas](images/fase-2/tema.jpeg "image_tooltip")

Figura 1: Tela de Escolha dos Temas.

### Proposições

Após escolher o tema e o período, exibe-se a lista de proposições. Vale destacar que o usuário poderá buscar uma proposição usando expressão regular. Também ao clicar no ícone de filtro, leva-se à tela de temas exibida anterioremente.

![Tela exibindo a lista de proposições](images/fase-2/proposicoes.jpeg "image_tooltip")

Figura 2: Tela exibindo a lista de proposições.

### Informações deputados e partidos

O usuário, ao clicar no card do depudato ou partido, poderá visualizar as informações do deputado/partido.

![Tela exibindo informações do deputado](images/fase-2/detalhes.jpeg "image_tooltip")

Figura 3: Tela exibindo informações do deputado.

### Visualizar votos dos deputados e partidos

Clicando no botão detalhes na tela anterior, abre-se uma webview explicando como cada deputado votou para as determinadas proposições.

![Tela mostrando os votos do deputado escolhido](images/fase-2/votacao.png "image_tooltip")

Figura 4: Tela mostrando os votos do deputado escolhido.