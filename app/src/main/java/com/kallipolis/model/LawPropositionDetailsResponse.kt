package com.kallipolis.model

import com.google.gson.annotations.SerializedName

data class LawPropositionDetailsResponse(
    @SerializedName("ano")
    val year: Int,
    @SerializedName("autores")
    val lawPropositionAuthors: List<LawPropositionAuthor>,
    @SerializedName("dataApresentacao")
    val presentationDate: String,
    @SerializedName("descricaoTipo")
    val typeDescription: String,
    @SerializedName("ementa")
    val scope: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("keywords")
    val keywords: String,
    @SerializedName("numero")
    val number: Int,
    @SerializedName("siglaTipo")
    val typeInitials: String,
    @SerializedName("statusProposicao")
    val lawPropositionStatus: LawPropositionStatus,
    @SerializedName("temas")
    val lawPropositionThemeDetails: List<LawPropositionThemeDetails>,
    @SerializedName("uri")
    val uri: String
)